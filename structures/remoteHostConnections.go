package structures

import (
	"os"
)

// RemoteConnectConfig provides structure for remote connections
type RemoteConnectConfig struct {
	User           string
	PrivateKeyPath string
	HostSSHKey     string
	HostPath       string
	HostName       string
}

// RemoteResult provides structure for stdout/err feedback
type RemoteResult struct {
	Err    error
	Stdout string
	Stderr string
}

var HetznerRemoteConnectConfig = &RemoteConnectConfig{
	User:           "root",
	PrivateKeyPath: "/root/.ssh/id_ed25519",
	HostSSHKey:     "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAUMGLJE6nChLRre0lL8Oxklg1irbXc6gUe+CFrfOYi1V6IXcDyu7gTIMJomylHhkQQdLgZPTO1Gs6RVOdx6Ljw=",
	HostPath:       "/root",
	HostName:       os.Getenv("HETZNER_HOSTNAME"),
}

var CakeforRemoteConnectConfig = &RemoteConnectConfig{
	User:           "ackersond",
	PrivateKeyPath: "/root/.ssh/id_ed25519",
	HostSSHKey:     "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJISb3hDRFPK4bCTH3Th7NThaZarvObOimfIVjKidQ2ttuSlCtCuwdShm4yyIc29c0NH4l2OYRYpTuene9RaEIo=",
	HostPath:       "/home/ackersond/",
	HostName:       os.Getenv("CAKEFOR_HOSTNAME"),
}

var ChoreRemoteConnectConfig = &RemoteConnectConfig{
	User:           "ackersond",
	PrivateKeyPath: "/root/.ssh/id_ed25519",
	HostSSHKey:     "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAjyr1P4Fr3kfN9f4xUncOpGJjC0G12qU8CDMgiB1nDwEjFmCTXsMF2US2kdW9BHLlOAi/KTu5E555hoKryDRSU=",
	HostPath:       "/home/ackersond/",
	HostName:       os.Getenv("CHORE_HOSTNAME"),
}

var ThorRemoteConnectConfig = &RemoteConnectConfig{
	User:           "ackersond",
	PrivateKeyPath: "/root/.ssh/id_ed25519",
	HostSSHKey:     "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJT4L1dyKiQW8DlG3FICssTOE3uU1Der+5H8k2o9+wGmr7JFji+S4ZfmX+P36ZrXfYeg76OgdBnNB6AjR2f9Pys=",
	HostPath:       "/home/ackersond/",
	HostName:       os.Getenv("THOR_HOSTNAME"),
}

var TuxedoRemoteConnectConfig = &RemoteConnectConfig{
	User:           "ackersond",
	PrivateKeyPath: "/root/.ssh/id_ed25519",
	HostSSHKey:     "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHidO00imLhVoy7mZqZxK6LDzH9NAZJxhPOwmZFA5K1BxnFPiIBhloc3Qhtu7h0+0sFNvcURLqaDp6+LAuIrwzk=",
	HostPath:       "/home/ackersond/",
	HostName:       os.Getenv("TUXEDO_HOSTNAME"),
}

var NASRemoteConnectConfig = &RemoteConnectConfig{
	User:           "ackersond",
	PrivateKeyPath: "/root/.ssh/id_ed25519",
	HostSSHKey:     "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBGU0HwYJOYIHUU/9ImoeKdrRg1BjxvAuhVsYnOhCZket9KkWrPYgmzucCVjD1qnULXBZiAdTmbEG1+X153jB+is=",
	HostPath:       "/home/ackersond/",
	HostName:       os.Getenv("NAS_HOSTNAME"),
}
