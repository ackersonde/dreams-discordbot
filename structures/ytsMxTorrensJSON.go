package structures

type YtsMx struct {
	Status        string `json:"status"`
	StatusMessage string `json:"status_message"`
	Data          struct {
		MovieCount int `json:"movie_count"`
		Limit      int `json:"limit"`
		PageNumber int `json:"page_number"`
		Movies     []struct {
			ID                      int      `json:"id"`
			URL                     string   `json:"url"`
			ImdbCode                string   `json:"imdb_code"`
			Title                   string   `json:"title"`
			TitleEnglish            string   `json:"title_english"`
			TitleLong               string   `json:"title_long"`
			Slug                    string   `json:"slug"`
			Year                    int      `json:"year"`
			Rating                  float64  `json:"rating"`
			Runtime                 int      `json:"runtime"`
			Genres                  []string `json:"genres"`
			Summary                 string   `json:"summary"`
			DescriptionFull         string   `json:"description_full"`
			Synopsis                string   `json:"synopsis"`
			YtTrailerCode           string   `json:"yt_trailer_code"`
			Language                string   `json:"language"`
			MpaRating               string   `json:"mpa_rating"`
			BackgroundImage         string   `json:"background_image"`
			BackgroundImageOriginal string   `json:"background_image_original"`
			SmallCoverImage         string   `json:"small_cover_image"`
			MediumCoverImage        string   `json:"medium_cover_image"`
			LargeCoverImage         string   `json:"large_cover_image"`
			State                   string   `json:"state"`
			Torrents                []struct {
				URL              string `json:"url"`
				Hash             string `json:"hash"`
				Quality          string `json:"quality"`
				Type             string `json:"type"`
				IsRepack         string `json:"is_repack"`
				VideoCodec       string `json:"video_codec"`
				BitDepth         string `json:"bit_depth"`
				AudioChannels    string `json:"audio_channels"`
				Seeds            int    `json:"seeds"`
				Peers            int    `json:"peers"`
				Size             string `json:"size"`
				SizeBytes        int64  `json:"size_bytes"`
				DateUploaded     string `json:"date_uploaded"`
				DateUploadedUnix int    `json:"date_uploaded_unix"`
			} `json:"torrents"`
			DateUploaded     string `json:"date_uploaded"`
			DateUploadedUnix int    `json:"date_uploaded_unix"`
		} `json:"movies"`
	} `json:"data"`
	Meta struct {
		ServerTime     int    `json:"server_time"`
		ServerTimezone string `json:"server_timezone"`
		APIVersion     int    `json:"api_version"`
		ExecutionTime  string `json:"execution_time"`
	} `json:"@meta"`
}
