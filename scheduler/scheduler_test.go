package scheduler

import (
	"testing"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/go-co-op/gocron/v2"
)

func TestStartScheduler(t *testing.T) {
	t.Run("creates scheduler with correct timezone", func(t *testing.T) {
		s := &discordgo.Session{}
		scheduler, err := StartScheduler(s)
		if err != nil {
			t.Errorf("StartScheduler() error = %v, want nil", err)
		}
		defer scheduler.Shutdown()

		// Verify the scheduler is using Europe/Berlin timezone
		loc, err := time.LoadLocation("Europe/Berlin")
		if err != nil {
			t.Fatal(err)
		}

		// Get a job's next run time to verify timezone
		jobs := scheduler.Jobs()
		if len(jobs) == 0 {
			t.Fatal("Expected at least one job to be scheduled")
		}

		nextRun, err := jobs[0].NextRun()
		if err != nil {
			t.Fatal(err)
		}

		if nextRun.Location().String() != loc.String() {
			t.Errorf("Scheduler timezone = %v, want %v", nextRun.Location(), loc)
		}
	})
}

func TestCron(t *testing.T) {
	t.Run("creates job with correct parameters", func(t *testing.T) {
		localTime, _ := time.LoadLocation("Europe/Berlin")
		scheduler, err := gocron.NewScheduler(gocron.WithLocation(localTime))
		if err != nil {
			t.Fatal(err)
		}
		scheduler.Start()
		defer scheduler.Shutdown()

		testCmd := func() {
			// This will be called when the job executes
		}

		cronName := "test-job"
		cronString := "*/5 * * * *" // every 5 minutes

		cron(cronName, cronString, scheduler, testCmd)

		// Verify job was created
		jobs := scheduler.Jobs()
		if len(jobs) != 1 {
			t.Errorf("Expected 1 job, got %d", len(jobs))
		}

		job := jobs[0]
		if job.Name() != cronName {
			t.Errorf("Job name = %v, want %v", job.Name(), cronName)
		}
	})
}

func TestPrintJobInfo(t *testing.T) {
	t.Run("handles error case", func(t *testing.T) {
		// Create a real scheduler to get a job for testing
		localTime, _ := time.LoadLocation("Europe/Berlin")
		scheduler, err := gocron.NewScheduler(gocron.WithLocation(localTime))
		if err != nil {
			t.Fatal(err)
		}

		job, err := scheduler.NewJob(
			gocron.CronJob("* * * * *", false),
			gocron.NewTask(func() {}),
			gocron.WithName("test-job"),
		)
		if err != nil {
			t.Fatal(err)
		}

		// Test successful case
		printJobInfo(job, nil)

		// Test error case
		printJobInfo(job, gocron.ErrJobNotFound)
	})
}
