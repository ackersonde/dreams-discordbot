package scheduler

import (
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/go-co-op/gocron/v2"
	"gitlab.com/ackersonde/dreams-discordbot/commands"
)

var cronChannel string

// StartScheduler initializes and starts the scheduler for periodic tasks
func StartScheduler(s *discordgo.Session) (gocron.Scheduler, error) {
	// Create a new scheduler
	localTime, _ := time.LoadLocation("Europe/Berlin") // use my time
	scheduler, err := gocron.NewScheduler(gocron.WithLocation(localTime))
	if err != nil {
		return scheduler, err
	}
	scheduler.Start()

	cronChannel = os.Getenv("CRONJOBS_CH_ID")
	addCronJobs(scheduler, s)

	return scheduler, nil
}

type cronCmd func()

func addCronJobs(scheduler gocron.Scheduler, s *discordgo.Session) {
	cron("check-disk-space", "00 07 * * *", scheduler,
		func() {
			commands.CheckDiskSpaceCron(s, cronChannel)
		})
	cron("check-backups", "05 07 * * *", scheduler,
		func() {
			commands.CheckBackupsCron(s, cronChannel)
		})
	cron("check-firewall-rules", "55 06 * * *", scheduler,
		func() {
			commands.CheckFirewallRulesCron(s, cronChannel)
		})

	/*cron("baseball-games", "30 17 * * *", scheduler,
	func() {
		commands.ShowBBGamesCron(s, cronChannel)
	})
	*/
}

func cron(cronName string, cronString string, scheduler gocron.Scheduler, cmd cronCmd) {
	job, err := scheduler.NewJob(
		gocron.CronJob(cronString, false),
		gocron.NewTask(cmd),
		gocron.WithName(cronName),
	)

	printJobInfo(job, err)
}

func printJobInfo(job gocron.Job, err error) {
	if err != nil {
		commands.Logger.Printf("Unable to add job: %s\n", err)
	} else {
		nextRun, err := job.NextRun()
		if err != nil {
			commands.Logger.Printf("Unable to find job details: %s\n", err)
		} else {
			commands.Logger.Printf(
				"Started %s[%s] will run @ %v in channel %s\n",
				job.Name(), job.ID().String(), nextRun, cronChannel)
		}
	}
}
