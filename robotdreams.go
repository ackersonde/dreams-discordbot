package main

import (
	"embed"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"gitlab.com/ackersonde/dreams-discordbot/commands"
	"gitlab.com/ackersonde/dreams-discordbot/scheduler"
)

// Edit your bot here: https://discord.com/developers/applications/

//go:embed static/comfyui.json
var comfyUIworkflowJSON embed.FS

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Printf("unable to load .env: %s\n", err.Error())
	}

	var Token = os.Getenv("APP_TK")
	var App = os.Getenv("APP_ID")
	var Guild = os.Getenv("SERVER_ID")

	session, err := discordgo.New("Bot " + Token)
	if err != nil {
		log.Printf("unable to start session: %s", err.Error())
	}

	comfyUIworkflowRAW, _ := comfyUIworkflowJSON.ReadFile("static/comfyui.json")
	commands.ComfyUIworkflowJSON = string(comfyUIworkflowRAW)

	createdCmds := registerCommands(session, App, Guild)
	session.AddHandler(commands.CommandHandler)

	// WARN: for the Bot to receive/read msgs, you must have all these things below:
	// - Administrator permissions to listen in on messages from this Server/Guild!
	// - "Message Content Intent" privs in the Gateway: https://discord.com/developers/
	// - all Intents set :/
	session.Identify.Intents = discordgo.IntentsAll
	session.AddHandler(commands.MessageHandler)

	// this message listener will essentially be the interface to ChatGPT AI
	session.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		log.Printf("Logged in as %s", r.User.String())
	})

	err = session.Open()
	if err != nil {
		log.Fatalf("could not open session: %s", err)
	}

	// setup cron jobs
	cron, err := scheduler.StartScheduler(session)
	if err != nil {
		log.Fatal("Error starting scheduler:", err)
	}

	sigch := make(chan os.Signal, 1)
	signal.Notify(sigch, syscall.SIGINT, syscall.SIGTERM, syscall.SIGSEGV, os.Interrupt) // k8s sends SIGTERM to pods
	<-sigch

	defer session.Close()
	for _, v := range createdCmds {
		err := session.ApplicationCommandDelete(session.State.User.ID, Guild, v.ID)
		if err != nil {
			log.Printf("Cannot delete '%s[%s]' command: %v", v.Name, v.ID, err)
		} else {
			log.Printf("Removed command '%s[%s]'...", v.Name, v.ID)
		}
		time.Sleep(2 * time.Second) // avoid rate limiting from discord!
	}
	cron.Shutdown()

	log.Println("Graceful shutdown")
}

// Be a lot more deliberate adding cmds to the discord bot here
// as the ApplicationCommandBulkOverwrite seems to silently fail
// 50% of the time in Kubernetes (which may not be removing all cmds correctly on pod shutdown?)
// Start off by **deleting** any/all existing cmds!
// Then register known cmds and double check if the count is right.
// Finally, if the count isn't right, rinse & repeat above steps until it is!
func registerCommands(session *discordgo.Session, App string, Guild string) []*discordgo.ApplicationCommand {
	currentCmds, err := session.ApplicationCommands(App, Guild)
	if err == nil && currentCmds != nil {
		for _, cmd := range currentCmds {
			err = session.ApplicationCommandDelete(App, Guild, cmd.ID)
			if err != nil {
				log.Printf("unable to delete %s[%s]: %s\n", cmd.Name, cmd.ID, err.Error())
			} else {
				log.Printf("deleted %s[%s]\n", cmd.Name, cmd.ID)
			}
			time.Sleep(2 * time.Second)
		}
	}

	for _, cmd := range commands.Commands {
		createdCmd, err := session.ApplicationCommandCreate(App, Guild, cmd)
		if err != nil {
			log.Fatalf("could not register command: %s", err)
		} else {
			log.Printf("registered %s[%s]\n", createdCmd.Name, createdCmd.ID)
		}
		time.Sleep(2 * time.Second)
	}

	createdCmds, err := session.ApplicationCommands(App, Guild)
	if err != nil {
		log.Fatalf("Couldn't retrieve cmds: %s", err.Error())
	} else {
		log.Printf("Successfully registered %d cmds", len(createdCmds))
	}
	if len(createdCmds) != len(commands.Commands) {
		log.Printf("Reregistering cmds as it was incomplete!!!")
		return registerCommands(session, App, Guild)
	}

	return createdCmds
}
