# dreams-discordbot

## Introduction

Edit your Discord bot [here](https://discord.com/developers/applications/1256900262162075648/bot).
TL;DR: You must give it Admin permissions otherwise it can't read messages from channels :/

Find examples [here](https://github.com/bwmarrin/discordgo/tree/master/examples).

<img src="https://juststickers.in/wp-content/uploads/2016/07/go-programming-language.png" width="20%">![Plus](https://icons.iconarchive.com/icons/icons8/ios7/64/User-Interface-Plus-icon.png) <img src="https://logos-world.net/wp-content/uploads/2020/12/Discord-Logo.png" width="30%">![Equals](https://icons.iconarchive.com/icons/fa-team/fontawesome/48/FontAwesome-Equals-icon.png) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <img src="https://filmforum.org/do-not-enter-or-modify-or-erase/client-uploads/ROBOT_DREAMS_thumbnail.png" height="30%" width="30%">

## Future ideas

