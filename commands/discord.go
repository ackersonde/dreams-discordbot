package commands

import (
	"os"
	"slices"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func prepPrivateMessage(s *discordgo.Session, reqUser *discordgo.User, reqChannelID string) (postChannel *discordgo.Channel) {
	var err error

	// DM
	cleanName := strings.ToLower(reqUser.GlobalName)
	cleanName = strings.ReplaceAll(cleanName, " ", "-")
	dmChannelName := "gpt-" + cleanName + "-dm"

	guilds, _ := s.GuildChannels(os.Getenv("SERVER_ID"))
	for _, channel := range guilds {
		if channel.Name == dmChannelName {
			postChannel = channel
			break
		}
	}

	if postChannel == nil {
		postChannel, err = s.GuildChannelCreateComplex(os.Getenv("SERVER_ID"),
			discordgo.GuildChannelCreateData{
				Name: dmChannelName,
				Type: discordgo.ChannelTypeGuildText,
				Topic: "Ask any question e.g. \"How to cook spaghetti [[You are Massimo Botura]]\"" +
					"  All prompts handled by my home server running llama (<https://ollama.com/library/llama3.2>)." +
					"  Direct prompt to Anthropic's Claude 3.5 Sonnet by adding `@@` to the very end after any [[system prompt]]." +
					"  For text to image prompts, simply prefix your prompt with `t2i:`",
			})
		if err != nil {
			Logger.Println(err.Error())
		}

		// invite the user to the new channel
		invite, err := s.ChannelInviteCreate(postChannel.ID, discordgo.Invite{
			Inviter:    s.State.User,
			TargetUser: reqUser,
			Channel:    postChannel,
			MaxAge:     3600, // invite expires in 60 mins
			MaxUses:    1,    // 1 use only
			Temporary:  false,
		})
		if err != nil {
			Logger.Println(err.Error())
		}

		s.ChannelMessageSend(reqChannelID,
			"Cool, this is our first chat. :)\n"+
				"I've created a channel just for us to [continue the thread](https://discord.gg/invite/"+invite.Code+")!\n")
	}

	return postChannel
}

type optionMap = map[string]*discordgo.ApplicationCommandInteractionDataOption

func parseOptions(options []*discordgo.ApplicationCommandInteractionDataOption) (om optionMap) {
	om = make(optionMap)
	for _, opt := range options {
		om[opt.Name] = opt
	}
	return
}

func MessageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Channels the Bot is actively listening on...
	ROBOTDREAMS := os.Getenv("ROBOTDREAMS_CH_ID")
	allowedChannels := []string{ROBOTDREAMS}
	guilds, _ := s.GuildChannels(os.Getenv("SERVER_ID"))
	for _, channel := range guilds {
		if strings.HasPrefix(channel.Name, "gpt-") && strings.HasSuffix(channel.Name, "-dm") {
			allowedChannels = append(allowedChannels, channel.ID)
		}
	}

	//Logger.Printf("Got msg on %s from %s: %s", m.ChannelID, m.Author, m.Message.Content)

	// don't talk to yourself (or any other bots)
	if m.Author.Bot {
		return
	}

	// ignore discord system msgs starting with a backslash
	if strings.HasPrefix(m.Content, "\\") {
		return
	}

	prompt, _, err := parsePrompt(m.Content)
	if err != nil {
		Logger.Printf("Failed to parse prompt: %v", err)
		return
	}
	threadName := m.Content
	if len(m.Content) >= 30 {
		threadName = m.Content[0:25]
	}

	if m.GuildID == "" {
		if strings.HasPrefix(prompt, "t2i:") {
			replyToThread("Text-to-image generation is not yet supported inside BOT DMs. Please try again in a public channel.", m.ChannelID, s)
			return
		}

		if postChannel := prepPrivateMessage(s, m.Author, m.ChannelID); postChannel != nil {
			msg, _ := s.ChannelMessageSend(postChannel.ID, m.Content)
			thread, err := s.MessageThreadStartComplex(postChannel.ID, msg.ID, &discordgo.ThreadStart{
				Name:                threadName,
				AutoArchiveDuration: 60,    // mins thread is auto "archived"? (probably too fast for LLM chats - at least one day?)
				Invitable:           false, // so far no private LLM threads (DM to bot not possible atm)
				RateLimitPerUser:    10,    // i see no evidence of any rateLimits here
			})
			if err != nil {
				Logger.Println(err.Error())
			}

			client, model, req := initGPTRequest(s, thread)
			if err := chat(client, req, prompt, s, thread.ID, model); err != nil {
				Logger.Printf("Chat error: %v", err)
			}

			s.ChannelMessageSendReply(m.ChannelID,
				"Join me over @ <#"+msg.ID+"> to continue the discussion!", m.Reference())
		}

		return
	}

	// check if the channel is accepting bot commands
	ch, err := s.State.Channel(m.ChannelID)
	if err != nil {
		Logger.Println(err.Error())
	}

	if slices.Contains(allowedChannels, m.ChannelID) && !ch.IsThread() {
		// we're in an allowed channel for chatting, but not in a thread yet -> start a thread
		thread, err := s.MessageThreadStartComplex(m.ChannelID, m.ID, &discordgo.ThreadStart{
			Name:                threadName,
			AutoArchiveDuration: 60,    // mins thread is auto "archived"? (probably too fast for LLM chats - at least one day?)
			Invitable:           false, // so far no private LLM threads (DM to bot not possible atm)
			RateLimitPerUser:    10,    // i see no evidence of any rateLimits here
		})
		if err != nil {
			Logger.Println(err.Error())
		}

		if strings.HasPrefix(prompt, "t2i:") {
			prompt = strings.TrimPrefix(prompt[4:], " ")
			textToImage(prompt, thread, s)
		} else {
			client, model, req := initGPTRequest(s, thread)
			if err := chat(client, req, prompt, s, thread.ID, model); err != nil {
				Logger.Printf("Chat error: %v", err)
			}
		}
	} else if ch.IsThread() && slices.Contains(allowedChannels, ch.ParentID) {
		// we're in a thread w/i an allowed channel -> respond inline
		if strings.HasPrefix(prompt, "t2i:") {
			replyToThread("Text-to-image generation is not supported inside threads.", m.ChannelID, s)
		} else {
			client, model, req := initGPTRequest(s, ch)
			if err := chat(client, req, prompt, s, m.ChannelID, model); err != nil {
				Logger.Printf("Chat error: %v", err)
			}
		}
	} else {
		// we're not in an allowed channel -> redirect
		for _, user := range m.Mentions {
			if user.ID == os.Getenv("APP_ID") {
				_, err := s.ChannelMessageSendReply(m.ChannelID,
					"Meet me over on <#"+ROBOTDREAMS+"> to chat or DM me!", m.Reference())
				if err != nil {
					Logger.Printf("unable to reply to msg: %s", err.Error())
				}
				break
			}
		}
	}
}

func sendImageToThread(model, imageFileName, title, threadID string, s *discordgo.Session) {
	// open image file for reading
	imageFile, err := os.Open(imageFileName)
	if err != nil {
		Logger.Printf("Failed to open image: %s", err.Error())
	}

	_, err = s.ChannelMessageSendComplex(threadID, &discordgo.MessageSend{
		Content: "This is what I imagined for your prompt:",
		Files: []*discordgo.File{
			{
				Name:        imageFile.Name(),
				ContentType: "image/png",
				Reader:      imageFile,
			},
		},
		Embed: &discordgo.MessageEmbed{
			Title:       title,
			Description: "Generated via @" + model + "@",
			Color:       0xFF8C00,
		},
	})
	if err != nil {
		Logger.Printf("Failed to send renderedImg: %s", err.Error())
	}

	// cleanup
	err = os.Remove(imageFile.Name())
	if err != nil {
		Logger.Printf("Failed to remove image: %s", err.Error())
	}
}

var discordMaxMessageLength = 2000

func replyToThread(answer, threadID string, s *discordgo.Session) (err error) {
	if len(answer) < discordMaxMessageLength+1 {
		_, err = s.ChannelMessageSend(threadID, answer)
		return err
	}

	end := 0
	for start := 0; start < len(answer); start = end {
		end = start + discordMaxMessageLength
		if end > len(answer) {
			end = len(answer)
		}

		// now back up to the last line break so we don't break words
		for end > start && answer[end-1] != '\n' {
			end--
		}

		chunk := answer[start:end]
		if chunk == "" {
			break // This should almost never happen unless `answer` was mutated.
		}
		if _, err := s.ChannelMessageSend(threadID, chunk); err != nil {
			Logger.Printf("Failed to send message: %s", err.Error())
		}
	}

	return err
}
