package commands

import (
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/ackerson-de-go/baseball"
)

var baseballEmoji = "<:baseball:1299638361878958090>"
var tampaEmoji = "<:tampa:1299638536613400607>"

var BaseballCommand = discordgo.ApplicationCommand{
	Name:        "bb",
	Description: "Show baseball games from given date <YYYY-MM-DD> (default yesterday)",
	Options: []*discordgo.ApplicationCommandOption{{
		Name:        "date",
		Description: "Date to show games from",
		Type:        discordgo.ApplicationCommandOptionString,
		Required:    false,
	}}}

func handleBaseballAsync(options optionMap) (string, error) {
	dateString := ""
	if options["date"] != nil {
		dateString = options["date"].StringValue()
		gameDate, err := time.Parse("2006-01-02", dateString)
		if err != nil {
			return "", fmt.Errorf("couldn't figure out date '%s'. Try `help`", dateString)
		}
		dateString = gameDate.Format("2006/month_01/day_02")
	}
	return ShowBBGames(dateString), nil
}

type FavGames struct {
	FavGamesList []baseball.GameDay
	FavTeam      baseball.Team
}

type GameDay struct {
	Date         string
	ReadableDate string
	Games        map[int][]string
}

var homePageMap map[int]baseball.Team

func ShowBBGames(fromDate string) string {
	if fromDate == "" {
		yesterday := time.Now().AddDate(0, 0, -1)
		fromDate = yesterday.Format("2006/month_01/day_02")
	}
	response := ShowBaseBallGames(fromDate)
	result := "Ball games from " + response.ReadableDate + ":\n"

	for _, gameMetaData := range response.Games {
		homeTeamSuffix := ""
		if strings.Contains(gameMetaData[0], "Tampa") || strings.Contains(gameMetaData[4], "Tampa") {
			homeTeamSuffix = " " + tampaEmoji
		}

		watchURL := "[" + gameMetaData[0] + " @ " + gameMetaData[4] + "](https://ackerson.de/bbStream?url=" + url.QueryEscape(gameMetaData[10]) + ")"
		result += watchURL + homeTeamSuffix + "\n"
	}

	return result
}

func ShowBBGamesCron(session *discordgo.Session, channelID string) {
	result := ShowBBGames("")

	_, err := session.ChannelMessageSend(channelID, result)
	if err != nil {
		Logger.Printf("Error sending message: %v", err)
	}
}

func ShowBaseBallGames(fromDate string) baseball.GameDay {
	homePageMap = baseball.InitHomePageMap()

	offset := ""

	gameDayListing := baseball.GameDayListingHandler(fromDate, offset, homePageMap)

	return gameDayListing
}
