package commands

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
	"golang.org/x/crypto/ssh"
)

var piEmoji = "<:raspberry_pi:1301855718781223017>"

var PiCommand = discordgo.ApplicationCommand{
	Name:        "pi",
	Description: "Check status of miniPCs",
}

func handlePiAsync() (string, error) {
	response := measureCPUTemp()
	response += getAppVersions()

	return response, nil
}

func measureCPUTemp() string {
	hosts := []structures.RemoteConnectConfig{
		*structures.CakeforRemoteConnectConfig,
		*structures.ChoreRemoteConnectConfig,
		*structures.TuxedoRemoteConnectConfig,
		*structures.ThorRemoteConnectConfig,
		*structures.NASRemoteConnectConfig}

	result := "*CPUs* :thermometer:\n"
	for _, host := range hosts {
		// raspberryPIs
		measureCPUTempCmd := "((TEMP=`cat /sys/class/thermal/thermal_zone0/temp`/1000)); echo \"$TEMP\"C"

		remoteResult := executeRemoteCmd(measureCPUTempCmd, &host)
		if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
			result += "_" + host.HostName + "_: " + remoteResult.Stderr + "\n"
		} else {
			result += "_" + host.HostName + "_: *" + strings.TrimSuffix(remoteResult.Stdout, "\n") + "*\n"
		}
	}

	return result
}

func getAppVersions() string {
	result := "\n*APPs* :martial_arts_uniform:\n"

	hosts := []structures.RemoteConnectConfig{
		*structures.CakeforRemoteConnectConfig,
		*structures.ChoreRemoteConnectConfig,
		*structures.TuxedoRemoteConnectConfig,
		*structures.ThorRemoteConnectConfig,
		*structures.NASRemoteConnectConfig}

	for _, host := range hosts {
		result += "_" + host.HostName + "_: "
		remoteResult := executeRemoteCmd("docker --version", &host)
		result += remoteResult.Stdout
	}
	return result + "\n"
}

var BookmarksCommand = discordgo.ApplicationCommand{
	Name:        "www",
	Description: "List of important links",
}

func handleBookmarksAsync() (string, error) {
	homepage := baseballEmoji + ": [homepage](https://ackerson.de) | " + vaultEmoji + ": [vault](https://vault.ackerson.de/ui/) | "
	discordApps := discordEmoji + ": [discord apps](https://discord.com/developers/applications)\n"
	fritzBox := ":house:: [fritzbox](https://192.168.178.1/) | [afraid](https://freedns.afraid.org/dynamic/v2/)\n"
	cakefor := piEmoji + ": [syncthing](https://homesync.ackerson.de) | [photoprism](https://photos.ackerson.de/) | [test vault](https://backvault.ackerson.de/ui/)\n"
	thor := protonVPNEmoji + ": [qbit](https://qbt.ackerson.de/) | [jelly](https://jelly.ackerson.de/web/index.html#!/home.html)\n"
	tuxedo := "<:omv:1323611822875742269> [OMV](https://nas.ackerson.de/) | <:openwebui:1323611824284897310> [Open-WebUI](https://webui.ackerson.de)"
	response := homepage + discordApps + fritzBox + cakefor + thor + tuxedo

	return response, nil
}

func remoteConnectionConfiguration(config *structures.RemoteConnectConfig) *ssh.ClientConfig {
	hostKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(config.HostSSHKey))
	if err != nil {
		Logger.Printf("error parsing: %v", err)
	}

	signer := GetPublicCertificate(config.PrivateKeyPath)

	return &ssh.ClientConfig{
		User:            config.User,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.FixedHostKey(hostKey),
	}
}

func fetchHomeIPv6Prefix() string {
	response := ""
	remoteResult := execFritzCmd("IGDIP", "STATE")

	re := regexp.MustCompile(`(.*)NewIPv6Prefix (?P<prefix>.*)\nNewPrefixLength (?P<length>.*)\n(.*)`)
	matches := re.FindAllStringSubmatch(remoteResult, -1)
	names := re.SubexpNames()

	m := map[string]string{}
	if len(matches) > 0 {
		for i, n := range matches[0] {
			m[names[i]] = n
		}
		if len(m) > 1 {
			response = m["prefix"] + "/" + m["length"]
		}
	}

	return response
}

func execFritzCmd(action string, param string) string {
	response := ""

	cmd := fmt.Sprintf(
		"/home/ackersond/fritzBoxShell.sh %s %s", action, param)

	remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)
	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		response = remoteResult.Stdout
	}

	return strings.TrimSuffix(response, "\n")
}

// GetPublicCertificate retrieves it from the given privateKeyPath param
func GetPublicCertificate(privateKeyPath string) ssh.Signer {
	key, err := os.ReadFile(privateKeyPath)
	if err != nil {
		Logger.Printf("unable to read private key file: %v", err)
	}

	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		Logger.Printf("Unable to parse private key: %v", err)
	}

	cert, _ := os.ReadFile(privateKeyPath + "-cert.pub")
	pk, _, _, _, err := ssh.ParseAuthorizedKey(cert)
	if err != nil {
		Logger.Printf("unable to parse CA public key: %v", err)
		return nil
	}

	certSigner, err := ssh.NewCertSigner(pk.(*ssh.Certificate), signer)
	if err != nil {
		Logger.Printf("failed to create cert signer: %v", err)
		return nil
	}

	return certSigner
}

func boldPercentages(str string) string {
	var re = regexp.MustCompile(`(\d+)%`)
	return re.ReplaceAllString(str, "**$1%**")
}

func executeRemoteCmd(cmd string, config *structures.RemoteConnectConfig) structures.RemoteResult {
	defer func() { //catch or finally
		if err := recover(); err != nil { //catch
			Logger.Printf("ssh: connecting to `%s` threw -> %v\n", config.HostName, err)
		}
	}()

	remoteConfig := remoteConnectionConfiguration(config)
	if remoteConfig != nil && config.HostName != "" {
		sshClient := initialDialOut(config.HostName, remoteConfig)
		if sshClient != nil {
			session, err := sshClient.NewSession()
			if err != nil {
				Logger.Printf("unable to create SSH session: %s", err.Error())
				return structures.RemoteResult{
					Err:    err,
					Stdout: "",
					Stderr: "",
				}
			}
			defer session.Close()

			var stdoutBuf bytes.Buffer
			session.Stdout = &stdoutBuf
			var stderrBuf bytes.Buffer
			session.Stderr = &stderrBuf
			err = session.Run(cmd)

			errStr := ""
			if stderrBuf.String() != "" {
				errStr = strings.TrimSpace(stderrBuf.String())
			}

			return structures.RemoteResult{Err: err, Stdout: stdoutBuf.String(), Stderr: errStr}
		}
	}

	return structures.RemoteResult{}
}

func initialDialOut(hostname string, remoteConfig *ssh.ClientConfig) *ssh.Client {
	connectionString := fmt.Sprintf("%s:%s", hostname, "22")
	sshClient, errConn := ssh.Dial("tcp4", connectionString, remoteConfig)
	if errConn != nil { //catch
		Logger.Printf("ssh: Trying to connect to host `%s` failed w/ %s\n(%v)\n", connectionString, errConn.Error(), remoteConfig)
		return nil
	}

	return sshClient
}

func fetchErrantHomeFWRules(homeIPv6Prefix string) []string {
	authorizedIPs := []string{homeIPv6Prefix, "192.168.178.0/24"}

	hosts := []structures.RemoteConnectConfig{
		*structures.CakeforRemoteConnectConfig,
		*structures.ChoreRemoteConnectConfig,
		*structures.TuxedoRemoteConnectConfig,
		*structures.ThorRemoteConnectConfig,
		*structures.NASRemoteConnectConfig}

	result := []string{}
	for _, host := range hosts {
		cmd := "/usr/bin/sudo /usr/sbin/ip6tables -L -n | grep :22$ | awk '{print $4}'"

		res := executeRemoteCmd(cmd, &host)
		//Logger.Printf("authorizedIPs: %v\n", authorizedIPs)
		if res.Stdout == "" {
			result = append(result, "Firewall is NOT enabled for "+host.HostName+"!")
		} else {
			scanner := bufio.NewScanner(strings.NewReader(res.Stdout))
			interim := ""
			for scanner.Scan() {
				text := strings.TrimSuffix(scanner.Text(), "\n")
				if !contains(authorizedIPs, text) {
					interim += scanner.Text()
				}
			}
			if interim != "" {
				result = append(result, host.HostName+": "+interim)
			}
		}
	}

	Logger.Printf("Inappropriate home FW rules: %v\n", result)
	return result
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

func fetchHomeIPs() (ipv6Prefix string, ipv4 string) {
	fritz := execFritzCmd("DEVICEINFO", "STATE")
	ipv6Prefix = strings.TrimSuffix(strings.Split(fritz, "Neues Präfix: ")[1], "\n")

	// ipv4 lookup
	remoteResult := executeRemoteCmd("/usr/bin/curl -s https://ipv4.ackerson.de/ip", structures.ChoreRemoteConnectConfig)
	if remoteResult.Err != nil {
		ipv4 = remoteResult.Stderr + ": " + remoteResult.Err.Error()
	} else if remoteResult.Stderr != "" {
		ipv4 = remoteResult.Stderr
	} else {
		ipv4 = remoteResult.Stdout
	}
	return ipv6Prefix, ipv4

}

func getIPv6forHostname(hostname string) string {
	cmd := "nslookup -type=aaaa " + hostname + " | grep Address | tail -n +2"
	domainIPv6Bytes, _ := exec.Command("/bin/sh", "-c", cmd).Output()

	domainIPv6 := string(bytes.Trim(domainIPv6Bytes, "\n"))
	domainIPv6 = strings.TrimPrefix(domainIPv6, "Address: ")

	return domainIPv6
}
