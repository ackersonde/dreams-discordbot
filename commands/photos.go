package commands

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
)

var photosEmoji = "<:photos:1327928236142755870>"

var PhotosCommand = discordgo.ApplicationCommand{
	Name:        "photos",
	Description: "Grab photo albums from PhotoPrism",
}

// handlePhotosAsync is the new async version that returns a string result
func handlePhotosAsync() (string, error) {
	populatedAlbumsResponse := ""
	baseURL := os.Getenv("PHOTOS_DOMAIN")

	// step 0. get albums
	req, err := http.NewRequest("GET", baseURL+"/api/v1/albums", nil)
	if err != nil {
		Logger.Printf("Error creating request: %v", err)
		return "Error creating request", err
	}

	// TODO: once we get more than 10 albums, introduce pagination (make use of `offset`)
	q := req.URL.Query()
	q.Add("count", "10")
	q.Add("offset", "0")
	q.Add("type", "album")
	q.Add("linkCount", "1")
	req.URL.RawQuery = q.Encode()

	req.Header.Set("X-Auth-Token", os.Getenv("PHOTOS_PASSWORD"))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		Logger.Printf("Error making request: %v", err)
		return "Error fetching albums", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		Logger.Printf("Unexpected status code: %d", resp.StatusCode)
		return "Unexpected status code", err
	}

	var albums structures.PhotoPrismAlbums
	if err := json.NewDecoder(resp.Body).Decode(&albums); err != nil {
		Logger.Printf("Error decoding response: %v", err)
		return "Error decoding response", err
	}

	for _, album := range albums {
		Logger.Printf("Album %s: %v", album.UID, album.Slug)

		// Get album links
		linksReq, err := http.NewRequest("GET", baseURL+"/api/v1/albums/"+album.UID+"/links", nil)
		if err != nil {
			Logger.Printf("Error creating links request: %v", err)
			continue
		}
		linksReq.Header.Set("X-Auth-Token", os.Getenv("PHOTOS_PASSWORD"))

		linksResp, err := client.Do(linksReq)
		if err != nil {
			Logger.Printf("Error fetching links: %v", err)
			continue
		}
		defer linksResp.Body.Close()

		var links structures.PhotoPrismLinks
		if err := json.NewDecoder(linksResp.Body).Decode(&links); err != nil {
			Logger.Printf("Error decoding links: %v", err)
			continue
		}

		if len(links) > 0 {
			expirationDate := links[0].ModifiedAt.AddDate(0, 0, -1*links[0].Expires)
			if links[0].Expires == 0 || !(time.Now().After(expirationDate)) {
				expiringInDays := links[0].Expires / 3600 / 24
				expiryMessage := fmt.Sprintf("expiring %dd w/ ", expiringInDays)
				if expiringInDays == 0 {
					expiryMessage = ""
				}

				publicURL := fmt.Sprintf("%s/s/%s", baseURL, links[0].Token)
				views := links[0].Views

				populatedAlbumsResponse += fmt.Sprintf("[%s](%s) (%s%d views)\n", album.Slug, publicURL, expiryMessage, views)
			}
		}
	}

	if populatedAlbumsResponse == "" {
		populatedAlbumsResponse = "Couldn't find any albums :/"
	}

	return populatedAlbumsResponse, nil
}
