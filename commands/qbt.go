package commands

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
)

var qbtEmoji = "<:qbt:1310588622570917888>"

var QBTCommand = discordgo.ApplicationCommand{
	Name:        "qbt",
	Description: "(list|add|delete) torrents",
	Options: []*discordgo.ApplicationCommandOption{{
		Name:        "delete",
		Description: "Delete a torrent by hash",
		Type:        discordgo.ApplicationCommandOptionString,
	}, {
		Name:        "add",
		Description: "Start downloading a new magnet link to QBT",
		Type:        discordgo.ApplicationCommandOptionString,
	}, {
		Name:        "list",
		Description: "Show list of running torrents",
		Type:        discordgo.ApplicationCommandOptionString,
		// this is weird, but if you don't need/want addtl params
		// you have to provide a "dummy" Choice which will autofill as user hits <tab>|<enter>
		Choices: []*discordgo.ApplicationCommandOptionChoice{{
			Name:  "<all>",
			Value: "list",
		}},
	}},
}

func handleQBTAsync(options optionMap) (string, error) {
	builder := new(strings.Builder)

	switch {
	case options["delete"] != nil:
		builder.WriteString(deleteTorrent(options["delete"].StringValue()))
	case options["add"] != nil:
		builder.WriteString(addTorrents(options["add"].StringValue(), false))
	case options["list"] != nil:
		fallthrough
	default:
		result := fmt.Sprintf("%v\n", getTorrents())
		builder.WriteString(result)
	}

	return builder.String(), nil
}

func getTorrents() string {
	url := "https://" + os.Getenv("HOME_API_HOSTNAME") + "/qbt/get"
	response, errValue := callServer(url, "GET")

	return string(response) + "\n" + errValue
}

func addTorrents(hash string, paused bool) string {
	url := "https://" + os.Getenv("HOME_API_HOSTNAME") + "/qbt/add?hash=" + hash
	if paused {
		url += "&paused=true"
	}
	response, errValue := callServer(url, "POST")

	return string(response) + "\n" + errValue
}

func deleteTorrent(hash string) (result string) {
	url := "https://" + os.Getenv("HOME_API_HOSTNAME") + "/qbt/delete?hash=" + hash
	response, errValue := callServer(url, "POST")

	return string(response) + "\n" + errValue
}

func callServer(url, method string) ([]byte, string) {
	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return nil, err.Error()
	}

	req.Header.Add("X-API-TOKEN", os.Getenv("HOME_QBT_API_TOKEN"))

	resp, err := client.Do(req)
	if err != nil {
		return nil, err.Error()
	}
	defer resp.Body.Close()

	response, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err.Error()
	}
	return response, ""
}
