package commands

import (
	"strings"
	"testing"
	"unicode"

	"github.com/bwmarrin/discordgo"
	"github.com/sethvargo/go-password/password"
)

func TestEscapeDiscordFormatting(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "escapes all special characters",
			input:    "Hello *bold* _italic_ `code` ~strike~ >quote |pipe|",
			expected: "Hello \\*bold\\* \\_italic\\_ \\`code\\` \\~strike\\~ \\>quote \\|pipe\\|",
		},
		{
			name:     "handles empty string",
			input:    "",
			expected: "",
		},
		{
			name:     "handles string with no special characters",
			input:    "Hello123",
			expected: "Hello123",
		},
		{
			name:     "handles multiple consecutive special characters",
			input:    "**__~~",
			expected: "\\*\\*\\_\\_\\~\\~",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := escapeDiscordFormatting(tt.input)
			if result != tt.expected {
				t.Errorf("escapeDiscordFormatting() = %v, want %v", result, tt.expected)
			}
		})
	}
}

func TestPasswordCommand(t *testing.T) {
	t.Run("basic command structure", func(t *testing.T) {
		if PasswordCommand.Name != "password" {
			t.Errorf("PasswordCommand.Name = %v, want %v", PasswordCommand.Name, "password")
		}
		if PasswordCommand.Description != "Generate a password with customizable parameters" {
			t.Errorf("PasswordCommand.Description = %v, want %v", PasswordCommand.Description, "Generate a password with customizable parameters")
		}
		if len(PasswordCommand.Options) != 5 {
			t.Errorf("len(PasswordCommand.Options) = %v, want %v", len(PasswordCommand.Options), 5)
		}
	})

	t.Run("length option", func(t *testing.T) {
		option := findOption(PasswordCommand.Options, "length")
		if option == nil {
			t.Fatal("length option not found")
		}

		if option.Type != discordgo.ApplicationCommandOptionInteger {
			t.Errorf("length option type = %v, want %v", option.Type, discordgo.ApplicationCommandOptionInteger)
		}
		if option.Required {
			t.Error("length option should not be required")
		}
		expectedDesc := "`64`: Length of password"
		if option.Description != expectedDesc {
			t.Errorf("length option description = %v, want %v", option.Description, expectedDesc)
		}
	})

	t.Run("digits option", func(t *testing.T) {
		option := findOption(PasswordCommand.Options, "digits")
		if option == nil {
			t.Fatal("digits option not found")
		}

		if option.Type != discordgo.ApplicationCommandOptionInteger {
			t.Errorf("digits option type = %v, want %v", option.Type, discordgo.ApplicationCommandOptionInteger)
		}
		if option.Required {
			t.Error("digits option should not be required")
		}
		expectedDesc := "`10`: Number of digits [0-9]"
		if option.Description != expectedDesc {
			t.Errorf("digits option description = %v, want %v", option.Description, expectedDesc)
		}
	})

	t.Run("symbols option", func(t *testing.T) {
		option := findOption(PasswordCommand.Options, "symbols")
		if option == nil {
			t.Fatal("symbols option not found")
		}

		if option.Type != discordgo.ApplicationCommandOptionInteger {
			t.Errorf("symbols option type = %v, want %v", option.Type, discordgo.ApplicationCommandOptionInteger)
		}
		if option.Required {
			t.Error("symbols option should not be required")
		}
		// Check that the description includes the actual symbols from the password package
		if !strings.Contains(option.Description, "`10`: Number of symbols") {
			t.Errorf("symbols option description should contain default value and label, got: %v", option.Description)
		}
	})

	t.Run("onlylower option", func(t *testing.T) {
		option := findOption(PasswordCommand.Options, "onlylower")
		if option == nil {
			t.Fatal("onlylower option not found")
		}

		if option.Type != discordgo.ApplicationCommandOptionBoolean {
			t.Errorf("onlylower option type = %v, want %v", option.Type, discordgo.ApplicationCommandOptionBoolean)
		}
		if option.Required {
			t.Error("onlylower option should not be required")
		}
		expectedDesc := "`false`: Only lowercase?"
		if option.Description != expectedDesc {
			t.Errorf("onlylower option description = %v, want %v", option.Description, expectedDesc)
		}
	})

	t.Run("allowrepeat option", func(t *testing.T) {
		option := findOption(PasswordCommand.Options, "allowrepeat")
		if option == nil {
			t.Fatal("allowrepeat option not found")
		}

		if option.Type != discordgo.ApplicationCommandOptionBoolean {
			t.Errorf("allowrepeat option type = %v, want %v", option.Type, discordgo.ApplicationCommandOptionBoolean)
		}
		if option.Required {
			t.Error("allowrepeat option should not be required")
		}
		expectedDesc := "`false`: Allow repeating runes?"
		if option.Description != expectedDesc {
			t.Errorf("allowrepeat option description = %v, want %v", option.Description, expectedDesc)
		}
	})

	t.Run("default values in descriptions", func(t *testing.T) {
		tests := []struct {
			optionName string
			wantValue  string
		}{
			{"length", "64"},
			{"digits", "10"},
			{"symbols", "10"},
			{"onlylower", "false"},
			{"allowrepeat", "false"},
		}

		for _, tt := range tests {
			option := findOption(PasswordCommand.Options, tt.optionName)
			if option == nil {
				t.Errorf("option %s not found", tt.optionName)
				continue
			}
			if !strings.Contains(option.Description, tt.wantValue) {
				t.Errorf("option %s description should contain default value %s, got: %v",
					tt.optionName, tt.wantValue, option.Description)
			}
		}
	})
}

// Helper function to find an option by name
func findOption(options []*discordgo.ApplicationCommandOption, name string) *discordgo.ApplicationCommandOption {
	for _, opt := range options {
		if opt.Name == name {
			return opt
		}
	}
	return nil
}

func TestPasswordGeneration(t *testing.T) {
	tests := []struct {
		name        string
		length      int
		digits      int
		symbols     int
		onlyLower   bool
		allowRepeat bool
		wantErr     bool
	}{
		{
			name:        "default values",
			length:      64,
			digits:      10,
			symbols:     10,
			onlyLower:   false,
			allowRepeat: false,
			wantErr:     false,
		},
		{
			name:        "minimum length",
			length:      4,
			digits:      2,
			symbols:     2,
			onlyLower:   false,
			allowRepeat: true,
			wantErr:     false,
		},
		{
			name:        "only lowercase",
			length:      32,
			digits:      5,
			symbols:     5,
			onlyLower:   true,
			allowRepeat: false,
			wantErr:     false,
		},
		{
			name:        "no repeating characters",
			length:      20,
			digits:      5,
			symbols:     5,
			onlyLower:   false,
			allowRepeat: false,
			wantErr:     false,
		},
		{
			name:        "invalid: too many digits",
			length:      10,
			digits:      20,
			symbols:     5,
			onlyLower:   false,
			allowRepeat: false,
			wantErr:     true,
		},
		{
			name:        "invalid: too many symbols",
			length:      10,
			digits:      2,
			symbols:     20,
			onlyLower:   false,
			allowRepeat: false,
			wantErr:     true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, err := password.Generate(tt.length, tt.digits, tt.symbols, tt.onlyLower, tt.allowRepeat)
			if (err != nil) != tt.wantErr {
				t.Errorf("Generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				// Verify password length
				if len(result) != tt.length {
					t.Errorf("password length = %v, want %v", len(result), tt.length)
				}

				// Verify lowercase-only if specified
				if tt.onlyLower {
					if strings.ToLower(result) != result {
						t.Error("password contains uppercase characters when onlyLower=true")
					}
				}

				// Count digits
				digitCount := 0
				for _, c := range result {
					if c >= '0' && c <= '9' {
						digitCount++
					}
				}
				if digitCount < tt.digits {
					t.Errorf("password contains %d digits, want at least %d", digitCount, tt.digits)
				}

				// Count symbols (this is a simplified check)
				symbolCount := 0
				for _, c := range result {
					if !unicode.IsLetter(c) && !unicode.IsNumber(c) {
						symbolCount++
					}
				}
				if symbolCount < tt.symbols {
					t.Errorf("password contains %d symbols, want at least %d", symbolCount, tt.symbols)
				}

				// Check for repeating characters if not allowed
				if !tt.allowRepeat {
					seen := make(map[rune]bool)
					for _, c := range result {
						if seen[c] {
							t.Error("password contains repeating characters when allowRepeat=false")
							break
						}
						seen[c] = true
					}
				}
			}
		})
	}
}
