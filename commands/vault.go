package commands

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	vault "github.com/hashicorp/vault/api"
	auth "github.com/hashicorp/vault/api/auth/approle"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
)

var vaultEmoji = "<:vault:1292125075855642665>"

var VFACommand = discordgo.ApplicationCommand{
	Name:        "vfa",
	Description: "(list|get|put|update) MFA tokens in Vault",
	Options: []*discordgo.ApplicationCommandOption{{
		Name:        "list",
		Description: "Show list of managed MFA tokens in Vault",
		Type:        discordgo.ApplicationCommandOptionString,
		Required:    false, // this does nothing :/ but is clearer than `true` (which is a lie)
		// this is weird, but if you don't need/want addtl params
		// you have to provide a "dummy" Choice which will autofill as user hits <tab>|<enter>
		Choices: []*discordgo.ApplicationCommandOptionChoice{
			{
				Name:  "<all>",
				Value: "list",
			},
		}}, {
		Name:        "get",
		Description: "Retrieve MFA(s) for given token (fuzzy search)",
		Type:        discordgo.ApplicationCommandOptionString,
		Options: []*discordgo.ApplicationCommandOption{
			{
				Name:        "token_name",
				Description: "Vault token name",
				Type:        discordgo.ApplicationCommandOptionString,
				Required:    true,
			},
		}}, {
		Name:        "put",
		Description: "<keyName> <secret> => adds a new MFA token to Vault",
		Type:        discordgo.ApplicationCommandOptionString,
		Options: []*discordgo.ApplicationCommandOption{
			{
				Name:        "token_name",
				Description: "Name of the MFA token",
				Type:        discordgo.ApplicationCommandOptionString,
				Required:    true,
			},
			{
				Name:        "token_secret",
				Description: "Secret key for the MFA token",
				Type:        discordgo.ApplicationCommandOptionString,
				Required:    true,
			},
		}}, {
		Name:        "update",
		Description: "Update Vault role CIDRs on IP address changes",
		Type:        discordgo.ApplicationCommandOptionString,
	}},
}

var totpEngineName string

// handleVFAAsync is the new async version that returns a string result
func handleVFAAsync(s *discordgo.Session, i *discordgo.InteractionCreate, opts optionMap) (string, error) {
	if i.Member.User.ID == os.Getenv("DAN_USER_ID") {
		totpEngineName = "totp"
	}

	builder := new(strings.Builder)
	switch {
	case opts["get"] != nil:
		builder.WriteString(readTOTPCodeForKey(totpEngineName, opts["get"].StringValue()))
	case opts["update"] != nil:
		builder.WriteString(updateRoleCIDRs("single", "totp-mgmt", fetchHomeIPv6Prefix()))
	case opts["put"] != nil:
		builder.WriteString(putTOTPKeyForEngine(totpEngineName, opts["put"].StringValue()))
	case opts["list"] != nil:
		fallthrough
	default:
		result := fmt.Sprintf("%v\n", listTOTPKeysForEngine(totpEngineName))
		builder.WriteString(result)
	}

	result := builder.String()

	// If this is in the robotdreams channel, send the actual result to a private channel
	if i.ChannelID == os.Getenv("ROBOTDREAMS_CH_ID") {
		postChannel := prepPrivateMessage(s, i.Member.User, i.ChannelID)
		s.ChannelMessageSend(postChannel.ID, result)
		result = "This is sensitive stuff, so I've sent it to our private chat @ <#" + postChannel.ID + ">"
	}

	return result, nil
}

func readTOTPCodeForKey(totpEngineName string, keyName string) string {
	longVaultSession()

	response := ""
	code, err := totpVaultClient.Logical().Read(totpEngineName + "/code/" + keyName)

	if err != nil {
		if strings.Contains(err.Error(), "unknown key: "+keyName) {
			allKeys := listTOTPKeysForEngine(totpEngineName)
			// search thru remoteResults and attempt to do a case-insensitive match on keyname
			for _, key := range allKeys {
				lowerCaseKey := strings.ToLower(key)
				lowerCaseKeyName := strings.ToLower(keyName)

				if strings.Contains(lowerCaseKey, lowerCaseKeyName) {
					response += key + ": " + readTOTPCodeForKey(totpEngineName, key) + "\n"
				}
			}
			if response == "" {
				response = fmt.Sprintf("Unable to find keyName %s. Try looking at all keys w/ `vfa`\n", keyName)
			} else {
				response = fmt.Sprintf("Found the following keys:\n%s\n", response)
			}
		} else {
			response = fmt.Sprintf("unexpected error: %s\n", err.Error())
		}
	} else {
		response = "**" + code.Data["code"].(string) + "**"
	}

	return response
}

func putTOTPKeyForEngine(totpEngineName string, args string) string {
	longVaultSession()
	response := ""

	// Split args into keyName and secret
	parts := strings.SplitN(args, " ", 2)
	if len(parts) != 2 {
		return "Invalid format. Please provide both token name and secret."
	}
	keyName := parts[0]
	secret := parts[1]

	payload := map[string]interface{}{
		"key": secret,
	}
	_, err := totpVaultClient.Logical().Write(totpEngineName+"/keys/"+keyName, payload)

	if err != nil {
		response = fmt.Sprintf("Unable to add %s: %s", keyName, err.Error())
	} else {
		response = readTOTPCodeForKey(totpEngineName, keyName)
	}

	return response
}

func listTOTPKeysForEngine(totpEngineName string) []string {
	longVaultSession()
	var response []string
	keys, err := totpVaultClient.Logical().List(totpEngineName + "/keys")

	if err != nil {
		response = append(response, err.Error())
	} else {
		for _, key := range keys.Data["keys"].([]interface{}) {
			response = append(response, key.(string))
		}
	}

	return response
}

func updateRoleCIDRs(authRole string, updateRole string, CIDRs string) string {
	updateVaultClient := totpVaultClient

	if authRole == "totp-mgmt" {
		longVaultSession()
	} else {
		updateVaultClient = singleVaultLogin(authRole)
	}

	response := ""

	parts := strings.Split(CIDRs, ",")
	finalList := ""
	for _, part := range parts {
		finalList += part + ","
	}
	finalList += "172.19.0.0/24"

	payload := map[string]interface{}{
		"token_bound_cidrs": finalList,
	}
	_, err := updateVaultClient.Logical().Write("auth/approle/role/"+updateRole, payload)
	if err != nil {
		response = fmt.Sprintf("Unable to update %s's CIDRs to %s: %s\n", updateRole, finalList, err.Error())
	} else {
		response = fmt.Sprintf("Updated %s's CIDRS to `%s`\n", updateRole, CIDRs)
	}
	return response
}

var totpVaultClient *vault.Client

func longVaultSession() {
	if totpVaultClient != nil && totpVaultClient.Token() != "" {
		return
	}

	go renewToken()
	loginAttempts := 0
	for loginAttempts < 10 {
		loginAttempts += 1
		if totpVaultClient == nil || totpVaultClient.Token() == "" {
			time.Sleep(100 * time.Millisecond)
		} else {
			break
		}
	}
}

func singleVaultLogin(role string) *vault.Client {
	vaultClientLogin, _ := vault.NewClient(vault.DefaultConfig())
	vaultClientLogin.SetAddress(os.Getenv("VAULT_ADDR"))

	_, err := loginToVault(vaultClientLogin, role)

	if err != nil {
		Logger.Printf("unable to authenticate to Vault: %v\n", err)
	}

	return vaultClientLogin
}

// The Secret ID is a value that needs to be protected, so instead of the
// app having knowledge of the secret ID directly,
func loginToVault(vaultClientLogin *vault.Client, role string) (*vault.Secret, error) {
	// A combination of a Role ID and Secret ID is required to log in to Vault
	// with an AppRole.
	// First, let's get the role ID given to us by our Vault administrator.
	role = strings.ReplaceAll(role, "-", "_")
	roleID := os.Getenv("VAULT_" + role + "_APPROLE") // git_secrets_ackersonde_mgmt
	if roleID == "" {
		return nil, errors.New("no role ID was provided in VAULT_" + role + "_APPROLE env var")
	}

	// The Secret ID is a value that needs to be protected, so instead of the
	// app having knowledge of the secret ID directly, we have a trusted orchestrator (https://learn.hashicorp.com/tutorials/vault/secure-introduction?in=vault/app-integration#trusted-orchestrator)
	// give the app access to a short-lived response-wrapping token (https://www.vaultproject.io/docs/concepts/response-wrapping).
	// Read more at: https://learn.hashicorp.com/tutorials/vault/approle-best-practices?in=vault/auth-methods#secretid-delivery-best-practices
	secretID := &auth.SecretID{FromString: os.Getenv("VAULT_" + role + "_SECRET")}

	appRoleAuth, err := auth.NewAppRoleAuth(roleID, secretID)
	if err != nil {
		Logger.Println(fmt.Errorf("unable to initialize AppRole auth method: %w", err))
	} else {
		authInfo, err := vaultClientLogin.Auth().Login(context.Background(), appRoleAuth)
		if err != nil {
			Logger.Println(fmt.Errorf("%w", err))
			if strings.Contains(err.Error(), "Vault is sealed") {
				cmd := "/home/ackersond/vault/unseal_vault.sh"
				remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)

				if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
					Logger.Println(remoteResult.Stderr)
					err = errors.New(remoteResult.Stderr)
				} else {
					time.Sleep(5 * time.Second)
					authInfo, err = vaultClientLogin.Auth().Login(context.Background(), appRoleAuth)
				}
			}
		}

		if authInfo == nil {
			Logger.Printf("no auth info was returned after login")
		}

		return authInfo, err
	}

	return nil, err
}

// Once you've set the token for your Vault client, periodically renew lease
func renewToken() {
	if totpVaultClient == nil {
		totpVaultClient, _ = vault.NewClient(vault.DefaultConfig())
		totpVaultClient.SetAddress(os.Getenv("VAULT_ADDR"))
	}

	for {
		vaultLoginResp, err := loginToVault(totpVaultClient, "totp-mgmt")
		if err != nil {
			Logger.Printf("unable to authenticate to Vault: %v\n", err)
			break
		} else {
			tokenErr := manageTokenLifecycle(vaultLoginResp)
			if tokenErr != nil {
				Logger.Printf("unable to start managing token lifecycle: %v\n", tokenErr)
				break
			}
		}
	}
}

// Starts token lifecycle management. Returns only fatal errors as errors,
// otherwise returns nil so we can attempt login again.
func manageTokenLifecycle(token *vault.Secret) error {
	renew := token.Auth.Renewable // You may notice a different top-level field called Renewable. That one is used for dynamic secrets renewal, not token renewal.
	if !renew {
		Logger.Printf("Token is not configured to be renewable. Re-attempting login.")
		return nil
	}

	watcher, err := totpVaultClient.NewLifetimeWatcher(&vault.LifetimeWatcherInput{
		Secret:    token,
		Increment: 3600, // Learn more about this optional value in https://www.vaultproject.io/docs/concepts/lease#lease-durations-and-renewal
	})
	if err != nil {
		return fmt.Errorf("unable to initialize new lifetime watcher for renewing auth token: %w", err)
	}

	go watcher.Start()
	defer watcher.Stop()

	for {
		select {
		// `DoneCh` will return if renewal fails, or if the remaining lease
		// duration is under a built-in threshold and either renewing is not
		// extending it or renewing is disabled. In any case, the caller
		// needs to attempt to log in again.
		case err := <-watcher.DoneCh():
			if err != nil {
				Logger.Printf("Failed to renew token: %v. Re-attempting login.", err)
				return nil
			}
			// This occurs once the token has reached max TTL.
			Logger.Printf("Token can no longer be renewed. Re-attempting login.")
			return nil

		// Successfully completed renewal
		case renewal := <-watcher.RenewCh():
			duration, _ := renewal.Secret.TokenTTL()
			Logger.Printf("Renewed vault Token for addtl %s", duration)
		}
	}
}
