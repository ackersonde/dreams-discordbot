package commands

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/sethvargo/go-password/password"
)

var PasswordCommand = discordgo.ApplicationCommand{
	Name:        "password",
	Description: "Generate a password with customizable parameters",
	Options: []*discordgo.ApplicationCommandOption{
		{
			Name:        "length",
			Description: "`64`: Length of password",
			Type:        discordgo.ApplicationCommandOptionInteger,
			Required:    false,
		},
		{
			Name:        "digits",
			Description: "`10`: Number of digits [0-9]",
			Type:        discordgo.ApplicationCommandOptionInteger,
			Required:    false,
		},
		{
			Name:        "symbols",
			Description: "`10`: Number of symbols " + password.Symbols,
			Type:        discordgo.ApplicationCommandOptionInteger,
			Required:    false,
		},
		{
			Name:        "onlylower",
			Description: "`false`: Only lowercase?",
			Type:        discordgo.ApplicationCommandOptionBoolean,
			Required:    false,
		},
		{
			Name:        "allowrepeat",
			Description: "`false`: Allow repeating runes?",
			Type:        discordgo.ApplicationCommandOptionBoolean,
			Required:    false,
		},
	},
}

func escapeDiscordFormatting(s string) string {
	replacer := strings.NewReplacer(
		"*", "\\*",
		"_", "\\_",
		"`", "\\`",
		"~", "\\~",
		">", "\\>",
		"|", "\\|",
	)
	return replacer.Replace(s)
}

func handlePasswordAsync(s *discordgo.Session, user *discordgo.User, channelID string, options map[string]*discordgo.ApplicationCommandInteractionDataOption) (string, error) {

	// Default values
	length := int64(64)
	digits := int64(10)
	symbols := int64(10)
	onlyLower := false
	allowRepeat := false

	// Get options from the interaction
	for _, opt := range options {
		switch opt.Name {
		case "length":
			length = opt.IntValue()
		case "digits":
			digits = opt.IntValue()
		case "symbols":
			symbols = opt.IntValue()
		case "onlylower":
			onlyLower = opt.BoolValue()
		case "allowrepeat":
			allowRepeat = opt.BoolValue()
		}
	}

	if length < 20 {
		digits = length / 2
		symbols = digits
	}

	// Generate password
	response, err := password.Generate(int(length), int(digits), int(symbols), onlyLower, allowRepeat)
	if err != nil {
		response = fmt.Sprintf("Passwd params => length: **%d** | digits: **%d** | symbols: **%d** | onlyLower: **%t** | allowRepeat: **%t**\n", length, digits, symbols, onlyLower, allowRepeat)
		response += "Error generating password: " + err.Error()
	} else {
		response = escapeDiscordFormatting(response)
	}

	// if this is in a public channel, send the password privately
	channel, _ := s.State.Channel(channelID)
	Logger.Printf("Channel type: %v for channel ID: %s", channel.Type, channelID)
	isPrivateChannel := channel.Type == discordgo.ChannelTypeDM ||
		channel.Type == discordgo.ChannelTypeGuildPrivateThread ||
		strings.HasPrefix(channel.Name, "gpt-") && strings.HasSuffix(channel.Name, "-dm")

	if !isPrivateChannel {
		postChannel := prepPrivateMessage(s, user, channelID)
		response = "This is sensitive stuff, so I've sent it to our private chat @ <#" + postChannel.ID + ">"
	}

	return response, nil
}
