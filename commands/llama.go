package commands

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/richinsley/comfy2go/client"
	"github.com/tmc/langchaingo/llms"
	"github.com/tmc/langchaingo/llms/anthropic"
	"github.com/tmc/langchaingo/llms/openai"
)

var chatDelimeter = "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
var defaultSystemPrompt = "You are a helpful chatbot"

func parsePrompt(msgContent string) (string, string, error) {
	if msgContent == "" {
		return "", "", fmt.Errorf("empty message content")
	}

	// LLM Chats are free form but may end with an optional SystemRole/Persona string
	// e.g. How to make spaghetti [[you are Massimo Bottura]]
	promptAndSystemPrompt := strings.Split(msgContent, "[[")
	systemPrompt := defaultSystemPrompt

	if len(promptAndSystemPrompt) > 1 {
		systemPromptArray := strings.Split(promptAndSystemPrompt[1], "]]")
		if len(systemPromptArray) > 0 {
			systemPrompt = strings.TrimSpace(systemPromptArray[0])
		}
	} else {
		promptAndSystemPrompt = strings.Split(msgContent, "@@") // in case a LLM was specified
	}

	prompt := strings.TrimSpace(promptAndSystemPrompt[0])
	if prompt == "" {
		return "", "", fmt.Errorf("empty prompt after parsing")
	}

	return prompt, systemPrompt, nil
}

func parseClientConfig(prompt string) (llms.Model, string, error) {
	var llm llms.Model
	model := "claude-3-7-sonnet-latest"
	var err error

	if strings.HasSuffix(prompt, "@@openai@@") {
		model = "gpt-4o"
		apiKey := os.Getenv("OPENAI_API_KEY")
		if apiKey == "" {
			return nil, "error", fmt.Errorf("OPENAI_API_KEY not set")
		}
		llm, err = openai.New(
			openai.WithToken(apiKey),
			openai.WithModel(model),
		)
	} else {
		apiKey := os.Getenv("ANTHROPIC_API_KEY")
		if apiKey == "" {
			return nil, "error", fmt.Errorf("ANTHROPIC_API_KEY not set")
		}
		llm, err = anthropic.New(
			anthropic.WithBaseURL("https://api.anthropic.com/v1"),
			anthropic.WithToken(apiKey),
			anthropic.WithModel(model),
		)
	}
	/*
			Some future version of langchain will have reasoningCallback support
		  Although it's been merged to `main` branch, it's not yet released
			https://github.com/tmc/langchaingo/pull/1125/files#diff-c2ef5845bb119bcf80dca894ab71078ae10a6c13d42bace47757f55110261279
		else if strings.HasSuffix(prompt, "@@deepseek@@") { // use llama
			llm, err = ollama.New(
				ollama.WithModel("deepseek-r1:8b"),
				ollama.WithServerURL("https://ollama.ackerson.de/v1"),
			)
		}*/

	if err != nil {
		return nil, model, fmt.Errorf("failed to create LLM client: %w", err)
	}

	return llm, model, nil
}

// Init client and messages (incl. entire Thread conversation for context) to LLM API
func initGPTRequest(session *discordgo.Session, thread *discordgo.Channel) (llms.Model, string, []llms.MessageContent) {
	// add orig prompt + systemPrompt from root msg
	messages, err := session.ChannelMessages(thread.ParentID, 1, "", "", "")
	if err != nil {
		log.Printf("unable to get parent message: %s\n", err.Error())
		return nil, "", nil
	}
	if len(messages) == 0 {
		log.Printf("no parent message found")
		return nil, "", nil
	}
	originalPrompt, systemPrompt, err := parsePrompt(messages[0].Content)
	if err != nil {
		log.Printf("prompt parsing error: %s\n", err.Error())
		return nil, "", nil
	}

	llm, model, err := parseClientConfig(messages[0].Content)
	if err != nil {
		log.Printf("client config error: %s\n", err.Error())
		return nil, model, nil
	}

	chatMessages := []llms.MessageContent{
		llms.TextParts(llms.ChatMessageTypeSystem, systemPrompt),
		llms.TextParts(llms.ChatMessageTypeHuman, originalPrompt),
	}

	// now dig thru thread and carefully label & attach addtl conversation
	messages, err = session.ChannelMessages(thread.ID, 100, "", "", "")
	if err != nil {
		log.Printf("unable to get thread messages: %s\n", err.Error())
		return llm, model, chatMessages // Continue with just the original message
	}

	for _, reply := range messages {
		if reply.Content == "" {
			continue // Skip empty messages
		}

		// take care to remove delimiter and system prompt from LLM responses
		response := strings.Split(reply.Content, chatDelimeter)
		if len(response) == 0 || response[0] == "" {
			continue // Skip if split resulted in empty content
		}

		content := strings.TrimSpace(response[0])
		if content == "" {
			continue // Skip if content is empty after trimming
		}

		if reply.Author.ID == os.Getenv("APP_ID") { // BOT answers
			chatMessages = append(
				chatMessages,
				llms.TextParts(llms.ChatMessageTypeAI, content),
			)
		} else { // human context/questions
			chatMessages = append(
				chatMessages,
				llms.TextParts(llms.ChatMessageTypeHuman, content),
			)
		}
	}

	return llm, model, chatMessages
}

var ComfyUIworkflowJSON string

func textToImage(prompt string, channel *discordgo.Channel, s *discordgo.Session) {
	model := "flux1-dev-fp8 on ComfyUI"
	if strings.HasSuffix(prompt, "@@") { // TODO: add support for OpenAI's API for text-to-image generation
		model = "DALL-E 3"

		// TODO: add support for OpenAI's API for text-to-image generation
		replyToThread("DALL-E 3 text-to-image generation is not yet supported.", channel.ID, s)
		return
	}

	clientAddr := "192.168.178.69"
	clientPort := 8188

	// create a new Comfy2Go client
	c := client.NewComfyClient(clientAddr, clientPort, nil)

	// the Comfy2Go client needs to be in an initialized state before
	// we can create and queue graphs
	if !c.IsInitialized() {
		//log.Printf("Initialize Client with ID: %s\n", c.ClientID())
		err := c.Init()
		if err != nil {
			log.Println("Error initializing client:", err)
		}
	}

	graph, _, err := c.NewGraphFromJsonString(ComfyUIworkflowJSON)
	if err != nil {
		log.Println("Failed to get workflow graph from json file:", err)
	}

	// override the workflow prompt w/ given user prompt
	node := graph.GetNodeById(6)
	node.WidgetValues = interface{}([]interface{}{prompt}) // unable to figure out how to use this guy's API :/

	// queue the prompt and get the resulting image
	item, err := c.QueuePrompt(graph)
	if err != nil {
		log.Println("Failed to queue prompt:", err)
	}

	renderingMsg, err := s.ChannelMessageSend(channel.ID, "Rendering image...")
	if err != nil {
		log.Println("Failed to initialize rendering thread:", err)
	}

	var imageFile *os.File
	// continuously read messages from the QueuedItem until we get the "stopped" message type
	for continueLoop := true; continueLoop; {
		msg := <-item.Messages
		switch msg.Type {
		case "stopped":
			// if we were stopped for an exception, display the exception message
			qm := msg.ToPromptMessageStopped()
			if qm.Exception != nil {
				log.Println(qm.Exception)
			}
			continueLoop = false
		case "started":
			//qm := msg.ToPromptMessageStarted()
			//log.Printf("Start executing prompt ID %s\n", qm.PromptID)
		case "executing":
			//qm := msg.ToPromptMessageExecuting()
			// store the node's title so we can use it in the progress bar
			//currentNodeTitle = qm.Title
			//log.Printf("Executing Node: %d (%s)\n", qm.NodeID, currentNodeTitle)
		case "progress":
			// update our progress bar
			qm := msg.ToPromptMessageProgress()
			s.ChannelMessageEdit(channel.ID, renderingMsg.ID, fmt.Sprintf("Image render progress: %d%%", qm.Value*5))
			//log.Printf("Progress: %d%%\n", qm.Value*5)
		case "data":
			qm := msg.ToPromptMessageData()
			// data objects have the fields: Filename, Subfolder, Type
			// * Subfolder is the subfolder in the output directory
			// * Type is the type of the image temp/
			for k, v := range qm.Data {
				if k == "images" || k == "gifs" {
					for _, output := range v {
						img_data, err := c.GetImage(output)
						if err != nil {
							log.Println("Failed to get image:", err)
						}
						imageFile, err = os.Create(output.Filename)

						if err != nil {
							log.Println("Failed to write image:", err)
						}
						imageFile.Write(*img_data)
						imageFile.Close()
						log.Println("Got image: ", output.Filename)
					}
				}
			}
		}
	}

	sendImageToThread(
		model,
		imageFile.Name(),
		prompt[7:25],
		channel.ID, s)
}

func chat(
	llm llms.Model, messages []llms.MessageContent, prompt string,
	s *discordgo.Session, channelID string, model string) error {

	if llm == nil || s == nil {
		return fmt.Errorf("nil llm or session")
	}

	// Validate prompt
	if strings.TrimSpace(prompt) == "" {
		return fmt.Errorf("empty prompt")
	}

	// Validate messages
	if len(messages) < 2 {
		return fmt.Errorf("invalid message history")
	}

	// Add new prompt to messages if it's not the original prompt
	if prompt != messages[1].Parts[0].(llms.TextContent).Text {
		messages = append(messages, llms.TextParts(llms.ChatMessageTypeHuman, prompt))
	}

	// Create initial message before starting stream
	msg, err := s.ChannelMessageSend(channelID, "Thinking...")
	if err != nil {
		Logger.Printf("Failed to create initial message: %v", err)
		return err
	}
	if msg == nil {
		return fmt.Errorf("created message is nil")
	}

	type responseState struct {
		mu              sync.Mutex
		fullResponse    strings.Builder
		currentResponse string
		lastUpdate      time.Time
		bufferSize      int
		currentMsg      *discordgo.Message
	}

	const (
		minUpdateInterval = 500 * time.Millisecond
		maxUpdateInterval = 3 * time.Second
		minBufferSize     = 50
	)

	state := &responseState{
		currentMsg: msg,
	}
	updateTicker := time.NewTicker(minUpdateInterval)
	defer updateTicker.Stop()

	done := make(chan struct{})
	errChan := make(chan error)

	// Start streaming in a goroutine
	go func() {
		ctx := context.Background()
		streamingCallback := func(ctx context.Context, chunk []byte) error {
			state.mu.Lock()
			defer state.mu.Unlock()

			content := string(chunk)
			state.fullResponse.WriteString(content)
			state.currentResponse = state.fullResponse.String()
			state.bufferSize += len(content)

			// If message gets too long, create a new message
			if len(state.currentResponse) > 1900 {
				if _, err := s.ChannelMessageEdit(channelID, msg.ID, state.currentResponse); err != nil {
					Logger.Printf("Failed to finalize long message: %v", err)
				}

				// Create new message for continuation
				newMsg, err := s.ChannelMessageSend(channelID, "Continuing...")
				if err != nil {
					return fmt.Errorf("failed to create continuation message: %v", err)
				}
				state.currentMsg = newMsg
				state.fullResponse.Reset()
				state.currentResponse = ""
				state.lastUpdate = time.Now()
				state.bufferSize = 0
			}
			return nil
		}

		/*
				Some future version of langchaingo will support reasoningCallback. This is the commit
				https://github.com/tmc/langchaingo/pull/1125/files#diff-c2ef5845bb119bcf80dca894ab71078ae10a6c13d42bace47757f55110261279
				Then we can reintroduce the deepseek-r1:8b model.
			if model == "deepseek-r1:8b" {
				// TODO: add support for reasoningCallback
				_, err := llm.GenerateContent(ctx, messages, llms.WithStreamingReasoningFunc(reasoningCallback))
				if err != nil {
					errChan <- err
					return
				}
				close(done)
			} else {*/
		_, err := llm.GenerateContent(ctx, messages, llms.WithStreamingFunc(streamingCallback))
		if err != nil {
			errChan <- err
			return
		}
		close(done)
		//	}
	}()

	// Main loop to handle updates and completion
	for {
		select {
		case <-done:
			state.mu.Lock()
			// Add final delimiter and system prompt
			suffix := "\n\n" + chatDelimeter + "\n[[" + messages[0].Parts[0].(llms.TextContent).Text + "]] @" + model + "@\n"
			finalResponse := state.currentResponse + suffix
			state.mu.Unlock()
			if _, err := s.ChannelMessageEdit(channelID, state.currentMsg.ID, finalResponse); err != nil {
				Logger.Printf("Failed to send final update: %v", err)
			}
			return nil
		case err := <-errChan:
			errMsg := fmt.Sprintf("Stream error: %s\n", err.Error())
			if _, err := s.ChannelMessageEdit(channelID, msg.ID, errMsg); err != nil {
				Logger.Printf("Failed to edit error message: %v", err)
			}
			return err
		case <-updateTicker.C:
			state.mu.Lock()
			timeSinceLastUpdate := time.Since(state.lastUpdate)
			shouldUpdate := (state.bufferSize >= minBufferSize && timeSinceLastUpdate >= minUpdateInterval) ||
				timeSinceLastUpdate >= maxUpdateInterval

			if shouldUpdate && state.currentResponse != "" {
				if _, err := s.ChannelMessageEdit(channelID, state.currentMsg.ID, state.currentResponse); err != nil {
					Logger.Printf("Failed to update message: %v", err)
				}
				state.lastUpdate = time.Now()
				state.bufferSize = 0
			}
			state.mu.Unlock()
		}
	}
}
