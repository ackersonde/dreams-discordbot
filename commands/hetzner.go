package commands

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
	"gitlab.com/ackersonde/hetzner/hetznercloud"
)

var hetznerEmoji = "<:hetzner:1301855715853471754>"

var HetznerCommand = discordgo.ApplicationCommand{
	Name:        "hetzner",
	Description: "Show hetzner server details",
}
var FirewallCommand = discordgo.ApplicationCommand{
	Name:        "security",
	Description: "Show hetzner server firewall information",
}

func handleHetznerAsync() (string, error) {
	response := "No servers at " + hetznerEmoji
	servers := hetznercloud.ListAllServers()
	if len(servers) > 0 {
		response = "Found these " + hetznerEmoji + " server(s):\n"
	}

	for _, server := range servers {
		serverInfoURL := fmt.Sprintf("https://console.hetzner.cloud/projects/%s/servers/%d/graphs", os.Getenv("HETZNER_PROJECT"), server.ID)
		serverIPv6 := server.PublicNet.IPv6.IP.String()
		serverIPv4 := server.PublicNet.IPv4.IP.String()
		if strings.HasSuffix(serverIPv6, "::") {
			serverIPv6 += "1"
		}

		response += fmt.Sprintf("[%s] & %s => [%s](%s)\n",
			serverIPv6, serverIPv4, server.Status, serverInfoURL)

		remoteCmd := "uptime; uname -a"
		remoteResult := executeRemoteCmd(remoteCmd, structures.HetznerRemoteConnectConfig)
		response += remoteResult.Stdout
	}

	return response, nil
}

func CheckFirewallRulesCron(session *discordgo.Session, channelID string) {
	homeIPv6Prefix, homeIPv4, domainIPv6, errantHetznerFWRules, errantHomeFWRules := discoverCurrentIPsAndFirewallRules()

	result := checkFirewallRules(false, homeIPv6Prefix, homeIPv4, domainIPv6, errantHetznerFWRules, errantHomeFWRules)

	if result != "" {
		session.ChannelMessageSend(channelID, result)
	}
}

func fetchErrantHetznerFWRules(homeIPv6Prefix, homeIPv4 string) []string {
	var extraRules []string

	sshFWRules := hetznercloud.GetSSHFirewallRules()
	for _, rule := range sshFWRules {
		rule = strings.TrimSpace(rule)
		if strings.HasSuffix(rule, "/32") && rule != homeIPv4+"/32" {
			Logger.Printf("%s doesn't MATCH %s\n", rule, homeIPv4)
			extraRules = append(extraRules, rule)
		} else if strings.HasSuffix(rule, "/56") && rule != homeIPv6Prefix {
			Logger.Printf("%s doesn't MATCH %s\n", rule, homeIPv6Prefix)
			extraRules = append(extraRules, rule)
		}
	}

	return extraRules
}

// checkFirewallRules does a cross check of SSH access between
// cloud instances and home network, ensuring minimal connectivity
func checkFirewallRules(manuallyCalled bool, homeIPv6Prefix string, homeIPv4 string,
	domainIPv6 string, extras []string, homeFirewallRules []string) string {
	response := ""
	if len(extras) > 0 {
		response += hetznerEmoji + " [open to](https://console.hetzner.cloud/projects/" + os.Getenv("HETZNER_PROJECT") +
			"/firewalls/" + os.Getenv("HETZNER_FIREWALL") + "/rules) -> " +
			"`" + strings.Join(extras, "`, `") + "`" + " :rotating_light:\n"
	} else if manuallyCalled {
		response += hetznerEmoji + " allowed from `" + homeIPv6Prefix + "`,`" + homeIPv4 + "` :house:\n"
	}

	Logger.Printf("Home fw should NOT contain %s!\n", domainIPv6)

	if len(homeFirewallRules) > 0 {
		response += "\n:house: opened on -> `" + strings.Join(homeFirewallRules, "`, `") + "`" + " :rotating_light:"
	} else if manuallyCalled {
		response += "\n:house: allowed from `" + homeIPv6Prefix + "`"
	}

	return strings.TrimSuffix(response, "\n")
}

func discoverCurrentIPsAndFirewallRules() (string, string, string, []string, []string) {
	homeIPv6Prefix, homeIPv4 := fetchHomeIPs()
	response := ""

	errantFWRules := fetchErrantHetznerFWRules(homeIPv6Prefix, homeIPv4)
	response += checkHetznerFirewallApplied()

	domainIPv6 := getIPv6forHostname("ackerson.de")
	Logger.Printf("found %s for ipv6 homepage", domainIPv6)
	homeFirewallRules := fetchErrantHomeFWRules(homeIPv6Prefix)
	return homeIPv6Prefix, homeIPv4, domainIPv6, errantFWRules, homeFirewallRules
}

func checkHetznerFirewallApplied() string {
	servers := hetznercloud.ListAllServers()

	id, _ := strconv.Atoi(os.Getenv("HETZNER_FIREWALL"))
	response := hetznercloud.ApplyFirewall(id, "label=docker")

	if strings.Contains(response, "firewall_already_applied") {
		return ""
	} else {
		return "\nApplying :fire:wall to [" + strconv.Itoa(servers[0].ID) + "(https://console.hetzner.cloud/projects/" + os.Getenv("HETZNER_PROJECT") + "/servers/" + strconv.Itoa(servers[0].ID) + "/firewalls) :rotating_light:\n"
	}
}
