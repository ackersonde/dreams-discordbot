package commands

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

// getSnowflakeTime converts a Discord snowflake ID to a time.Time
func getSnowflakeTime(snowflake string) time.Time {
	// Discord Epoch (2015-01-01)
	discordEpoch := int64(1420070400000)

	// Convert snowflake to int64
	id, _ := strconv.ParseInt(snowflake, 10, 64)

	// Extract timestamp (first 42 bits, shifted right by 22 bits)
	timestamp := (id >> 22) + discordEpoch

	// Convert to time.Time (timestamp is in milliseconds)
	return time.UnixMilli(timestamp)
}

// Logger to give senseful settings
var Logger = log.New(os.Stdout, "", log.LstdFlags)

var discordEmoji = "<:discord:1312698961378807859>"

// Emoji mappings for commands
var emojis = map[string]string{
	"help":     ":palm_up_hand:",
	"vfa":      vaultEmoji,
	"qbt":      qbtEmoji,
	"search":   ":magnet:",
	"bb":       baseballEmoji,
	"crypto":   ethEmoji,
	"pgp":      pgpEmoji,
	"fsck":     ":floppy_disk:",
	"hetzner":  hetznerEmoji,
	"security": ":closed_lock_with_key:",
	"password": ":key:",
	"pi":       piEmoji,
	"www":      ":bookmark:",
	"photos":   photosEmoji,
}

var Commands = []*discordgo.ApplicationCommand{
	{
		Name:        "help",
		Description: "Show list of cmds and their options",
	},
	&PasswordCommand,
	&VFACommand,
	&SearchCommand,
	&PhotosCommand,
	&FirewallCommand,
	&QBTCommand,
	&BaseballCommand,
	&CrytpoCommand,
	&PGPCommand,
	&FSCKCommand,
	&HetznerCommand,
	&PiCommand,
	&BookmarksCommand,
}

// acknowledgeCommand sends an immediate response to acknowledge receipt of a command
func acknowledgeCommand(s *discordgo.Session, i *discordgo.InteractionCreate, cmdName string) error {
	return s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: "Processing `/" + cmdName + "` command...",
		},
	})
}

// executeCommandAsync handles the asynchronous execution of a command and sends the result
func executeCommandAsync(
	s *discordgo.Session, i *discordgo.InteractionCreate,
	data discordgo.ApplicationCommandInteractionData, handler func() (string, error)) {

	userID := i.Member.User.ID
	timestamp := getSnowflakeTime(i.Interaction.ID).Format("15:04:05")
	cmdName := data.Name
	cmdOptions := parseOptions(data.Options)

	go func() {
		result, err := handler()
		var message string
		if err != nil {
			message = "Error executing `/" + cmdName + "`: " + err.Error()
		} else {
			message = "Results for <@" + userID + ">'s `/" + cmdName
			if len(cmdOptions) > 0 {
				for k, v := range cmdOptions {
					var val string
					switch v.Type {
					case discordgo.ApplicationCommandOptionString:
						val = v.StringValue()
					case discordgo.ApplicationCommandOptionInteger:
						val = fmt.Sprintf("%d", v.IntValue())
					case discordgo.ApplicationCommandOptionBoolean:
						val = fmt.Sprintf("%v", v.BoolValue())
					default:
						val = fmt.Sprintf("%v", v.Value)
					}
					message += " " + k + ":" + val
				}
			}
			message += "` command [" + timestamp + "]:\n\n" + result
		}

		err = replyToThread(message, i.Interaction.ChannelID, s)
		if err != nil {
			Logger.Printf("Failed to send command result: %v", err)
		}
	}()
}

func CommandHandler(session *discordgo.Session, i *discordgo.InteractionCreate) {
	if i.Type != discordgo.InteractionApplicationCommand && i.Type != discordgo.InteractionMessageComponent {
		Logger.Printf("What's going on here? %v", i.Type)
		return
	}

	data := i.ApplicationCommandData()

	// Acknowledge receipt immediately
	if err := acknowledgeCommand(session, i, data.Name); err != nil {
		Logger.Printf("Failed to acknowledge command: %v", err)
		return
	}

	// Handle commands asynchronously
	switch data.Name {
	case "help":
		executeCommandAsync(session, i, data, func() (string, error) {
			builder := new(strings.Builder)
			for _, command := range Commands {
				emoji := emojis[command.Name]
				builder.WriteString(emoji + " **/" + command.Name + "**: " + command.Description + "\n")
			}
			builder.WriteString("<:claude:1336976850664165508> <text prompt> ([[<systemPrompt]]) (<:openai:1292123530414133330>@@openai@@)\n")
			builder.WriteString("<:flux:1294951977008103516> **t2i:** <image prompt> (flux1.dev via [comfyUI](http://192.168.178.69:8188/))\n")
			return builder.String(), nil
		})
	case "fotos":
		fallthrough
	case "photos":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handlePhotosAsync()
		})
	case "search":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleSearchAsync(parseOptions(data.Options))
		})
	case "passwd":
		fallthrough
	case "password":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handlePasswordAsync(session, i.Member.User, i.ChannelID, parseOptions(data.Options))
		})
	case "vfa":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleVFAAsync(session, i, parseOptions(data.Options))
		})
	case "pi":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handlePiAsync()
		})
	case "www":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleBookmarksAsync()
		})
	case "qbt":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleQBTAsync(parseOptions(data.Options))
		})
	case "bb":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleBaseballAsync(parseOptions(data.Options))
		})
	case "crypto":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleCryptoAsync()
		})
	case "pgp":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handlePGPAsync()
		})
	case "fsck":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleFSCKAsync()
		})
	case "hetzner":
		executeCommandAsync(session, i, data, func() (string, error) {
			return handleHetznerAsync()
		})
	case "security":
		executeCommandAsync(session, i, data, func() (string, error) {
			// Get current state
			homeIPv6Prefix, homeIPv4, domainIPv6, errantHetznerFWRules, errantHomeFWRules := discoverCurrentIPsAndFirewallRules()
			// Check state against what it should be
			return checkFirewallRules(true, homeIPv6Prefix, homeIPv4, domainIPv6, errantHetznerFWRules, errantHomeFWRules), nil
		})
	default:
		executeCommandAsync(session, i, data, func() (string, error) {
			return "", errors.New("command not found")
		})
	}
}
