package commands

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
)

var backupPath = "/mnt/usb2TB/"
var mediaPath = "/home/ackersond/NAS/DLNA"

var FSCKCommand = discordgo.ApplicationCommand{
	Name:        "fsck",
	Description: "Check storage status of media disks",
}

func handleFSCKAsync() (string, error) {
	response := boldPercentages(CheckMediaDiskSpace("") + CheckBackupDiskSpace("") + CheckServerDiskSpace("") + CheckHetznerSpace("/", true))

	issues := boldPercentages(checkDiskSpace() + checkBackups())
	if issues == "" {
		issues = ":sunglasses: None found :rainbow:"
	} else {
		issues = ":skull_and_crossbones: " + issues + " :skull_and_crossbones:"
	}

	return response + "\n" + issues, nil
}

func CheckServerDiskSpace(path string) string {
	if path != "" {
		path = strings.TrimSuffix(path, "/")
		if !strings.HasPrefix(path, "/") {
			path = "/" + path
		}
	}

	response := ""
	cmd := fmt.Sprintf("/bin/df -h -t ext4 %s", path)
	remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)
	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		for _, line := range strings.Split(
			strings.TrimSuffix(remoteResult.Stdout, "\n"), "\n") {
			if strings.Contains(line, "loop") ||
				strings.Contains(line, "tmpfs") {
				continue
			} else {
				response += "\n" + line
			}
		}
	}
	response += "\n==========================\n"

	return piEmoji + " *SD Card Disk Usage* `chore`\n" + response
}

func CheckMediaDiskSpace(path string) string {
	if path != "" {
		path = strings.TrimSuffix(path, "/")
		if !strings.HasPrefix(path, "/") {
			path = "/" + path
		}
	}

	response := ""
	cmd := fmt.Sprintf("/usr/bin/du -hs %s/* | sort -h", mediaPath+path)
	Logger.Printf("cmd: %s", cmd)
	remoteResult := executeRemoteCmd(cmd, structures.NASRemoteConnectConfig)

	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		response = remoteResult.Stdout
	}

	response = "<:jelly:1301855717560418337> USB *Disk Usage* `nas@" + mediaPath + path +
		"`\n" + response

	cmd = "df -h -t ext4 | sort -h"
	remoteResult = executeRemoteCmd(cmd, structures.NASRemoteConnectConfig)

	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response += "\n" + remoteResult.Stderr
	} else {
		response += "\n" + remoteResult.Stdout
	}
	response += "\n==========================\n"

	return response
}

func CheckBackupDiskSpace(path string) string {
	if path != "" {
		path = strings.TrimSuffix(path, "/")
		if !strings.HasPrefix(path, "/") {
			path = "/" + path
		}
	}

	response := ""
	cmd := fmt.Sprintf("/usr/bin/du -hs %s/* | sort -h", backupPath+path)
	Logger.Printf("cmd: %s", cmd)
	remoteResult := executeRemoteCmd(cmd, structures.TuxedoRemoteConnectConfig)

	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		response = remoteResult.Stdout
	}

	response = ":safety_vest: USB *Disk Usage* `tuxedo@" + backupPath + path +
		"`\n" + response

	cmd = "df -h -t ext4 | sort -h"
	remoteResult = executeRemoteCmd(cmd, structures.TuxedoRemoteConnectConfig)

	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response += "\n" + remoteResult.Stderr
	} else {
		response += "\n" + remoteResult.Stdout
	}
	response += "\n==========================\n"

	return response
}

func CheckDiskSpaceCron(session *discordgo.Session, channelID string) {
	result := checkDiskSpace()
	if result != "" {
		_, err := session.ChannelMessageSend(channelID, result)
		if err != nil {
			Logger.Printf("Error sending message: %v", err)
		}
	}
}

// Check disk space of important devices
func checkDiskSpace() string {
	response := ""
	response += checkDiskSpaceOfServer("nas", "/root")
	response += checkDiskSpaceOfServer("tuxedo", "/dev/sda1")
	response += checkDiskSpaceOfServer("tuxedo", "/dev/nvme1n1p1")
	response += checkDiskSpaceOfServer("chore", "/dev/sda1")
	response += checkDiskSpaceOfServer("chore", "/dev/mmcblk0p2")
	response += checkDiskSpaceOfServer("thor", "/dev/mmcblk0p2")
	response += checkDiskSpaceOfServer("hetzner", "/")
	response += checkDiskSpaceOfServer("hetzner", "/mnt/hetzner_disk")

	return response
}

func checkDiskSpaceOfServer(server string, mount string) string {
	response := ""
	sshConfig := structures.ChoreRemoteConnectConfig

	if server == "hetzner" {
		sshConfig = structures.HetznerRemoteConnectConfig
	} else if server == "chore" {
		sshConfig = structures.ChoreRemoteConnectConfig
	} else if server == "thor" {
		sshConfig = structures.ThorRemoteConnectConfig
	} else if server == "tuxedo" {
		sshConfig = structures.TuxedoRemoteConnectConfig
	} else if server == "nas" {
		sshConfig = structures.NASRemoteConnectConfig
	}

	cmd := fmt.Sprintf("df %s | sed 1d | awk '{ print $5 }'", mount)
	Logger.Printf("checkDiskSpace: %s", cmd)
	remoteResult := executeRemoteCmd(cmd, sshConfig)

	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		response = remoteResult.Stdout
		// take the resulting string and get it's numeric value e.g. "29%" => 29
		// if error || >= 95% report an error
		i, err := strconv.Atoi(strings.TrimRight(response, "%\n"))
		if err != nil {
			response = fmt.Sprintf("%s@%s: unable to parse %s: %s\n", server, mount, response, err)
		} else if i >= 92 { // only report if > 92%
			response = fmt.Sprintf("%s@%s: disk used *%d%%* :rotating_light:\n", server, mount, i)
		} else { // disk space is < 92% -> OK
			Logger.Printf("%s@%s: disk used %d%%\n", server, mount, i)
			response = "" // don't bother me
		}
	}

	return response
}

func CheckBackupsCron(session *discordgo.Session, channelID string) {
	result := checkBackups()

	if result != "" {
		_, err := session.ChannelMessageSend(channelID, result)
		if err != nil {
			Logger.Printf("Error sending message: %v", err)
		}
	}
}

func checkBackups() string {
	response := ""

	now := time.Now()
	//lastMonth := now.AddDate(0, -1, 0)
	calcDate := now.Format("2006/01/")

	response += checkBackupDirectory("nas", "/home/ackersond/NAS/backups/vault-secrets")
	response += checkBackupDirectory("tuxedo", "/mnt/usb2TB/backups/photos/"+calcDate)
	response += checkBackupDirectory("hetzner", "/mnt/hetzner_disk/backups/photos/"+calcDate)

	return response
}

func checkBackupDirectory(server string, path string) string {
	response := ""
	sshConfig := structures.ChoreRemoteConnectConfig

	if server == "hetzner" {
		sshConfig = structures.HetznerRemoteConnectConfig
	} else if server == "tuxedo" {
		sshConfig = structures.TuxedoRemoteConnectConfig
	} else if server == "nas" {
		sshConfig = structures.NASRemoteConnectConfig
	}

	cmd := fmt.Sprintf("ls -l %s | wc -l && du -sh %s", path, path)
	// e.g.
	// 108
	// 495M	/mnt/hetzner_disk/backups/photos/2022/05/

	Logger.Printf("checkBackups: %s", cmd)

	remoteResult := executeRemoteCmd(cmd, sshConfig)
	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		Logger.Printf("%s: %s", path, remoteResult.Stdout)
		scanner := bufio.NewScanner(strings.NewReader(remoteResult.Stdout))
		row := 0
		for scanner.Scan() {
			text := scanner.Text()
			if row == 0 {
				files, err := strconv.Atoi(text)
				if err != nil || files < 2 {
					response += "No files in `@" + server + "'s` backup dir `" + path + "`\n"
					break
				}
			} else if strings.HasPrefix(text, "4.0K") {
				response += "@" + server + "'s `" + path + "` exists, but currently empty\n"
			}
			row++
		}
	}

	return response
}

func CheckHetznerSpace(path string, showHeader bool) string {
	response := "<:hetzner:1301855715853471754> *Hetzner Disk Usage* @ `/`:\n"
	suffix := ""

	if !showHeader {
		suffix = " | sed 1d"
	}

	cmd := fmt.Sprintf("df -h -t ext4 %s%s", path, suffix)
	Logger.Printf("CheckHetznerSpace: %s", cmd)
	remoteResult := executeRemoteCmd(cmd, structures.HetznerRemoteConnectConfig)

	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		response = remoteResult.Stdout
	}

	return response
}
