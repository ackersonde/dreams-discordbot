package commands

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
)

var ethEmoji = "<:ethereum:1300456814869086309>"
var lumensEmoji = "<:lumens:1300461435343863922>"
var pgpEmoji = "<:pgp:1300456816756392050>"
var ubuntuEmoji = "<:ubuntu:1300474264184426536>"
var keybaseEmoji = "<:keybase:1300474262876061758>"

var CrytpoCommand = discordgo.ApplicationCommand{
	Name:        "crypto",
	Description: "Current cryptocurrency stats " + lumensEmoji,
}
var PGPCommand = discordgo.ApplicationCommand{
	Name:        "pgp",
	Description: "PGP keys",
}

func handleCryptoAsync() (string, error) {
	return checkEthereumValue() + "\n" + checkStellarLumensValue(), nil
}

func handlePGPAsync() (string, error) {
	var pgpKey = os.Getenv("PGP_FINGERPRINT")

	currentPGPKey := "[" + pgpKey[24:] + "](https://keyserver.ubuntu.com/pks/lookup?search=0x" + pgpKey + "&fingerprint=on&op=index)"
	pastPGPKeys := "['Dan Ackerson'](https://keyserver.ubuntu.com/pks/lookup?search=Dan+Ackerson&fingerprint=on&op=index)"

	keybaseIdentify := "[danackerson](https://keybase.io/danackerson)"
	keybasePGP := "[" + pgpKey[24:] + "](https://keybase.io/danackerson/pgp_keys.asc?fingerprint=" + pgpKey + ")"

	// TODO: time to delete keybase account?

	return fmt.Sprintf("%s %s & %s\n%s %s & %s",
		ubuntuEmoji, currentPGPKey, pastPGPKeys,
		keybaseEmoji, keybasePGP, keybaseIdentify), nil
}

func checkStellarLumensValue() string {
	response := ""

	accountBalanceStr := getStellarLumens()
	if !strings.HasPrefix(accountBalanceStr, "ERR") {
		stellarPriceUSDStr := getStellarPrice()
		if !strings.HasPrefix(stellarPriceUSDStr, "ERR") {
			accountBalance, err := strconv.ParseFloat(accountBalanceStr, 64)
			if err == nil {
				stellarPriceUSD, err := strconv.ParseFloat(stellarPriceUSDStr, 64)
				if err == nil {
					response = fmt.Sprintf(
						"Your %f %s is worth $%f [stellar.expert](https://stellar.expert/explorer/public/account/%s)",
						accountBalance, lumensEmoji, stellarPriceUSD*accountBalance, os.Getenv("LUMENS_ADDRESS"))
				} else {
					response = err.Error()
				}
			} else {
				response = err.Error()
			}
		} else {
			response = stellarPriceUSDStr
		}
	} else {
		response = accountBalanceStr
	}

	return response
}

func getStellarPrice() string {
	response := ""

	stellarPriceResp, err := http.Get("https://api.stellarterm.com/v1/ticker.json")
	if err != nil {
		response = fmt.Sprintf("ERR: stellar Lumens http.Get: %s", err)
	} else {
		defer stellarPriceResp.Body.Close()
		stellarPriceJSON, _ := io.ReadAll(stellarPriceResp.Body)
		// "externalPrices":{"USD_BTC":9650.16,"BTC_XLM":0.00000717,"USD_XLM":0.069192,"USD_XLM_24hAgo":0.070537,"USD_XLM_change":-1.90748}

		re := regexp.MustCompile(`"USD_XLM":(?P<price>[0-9]+\.?[0-9]*),`)
		matches := re.FindAllStringSubmatch(string(stellarPriceJSON), -1)
		names := re.SubexpNames()

		m := map[string]string{}
		for i, n := range matches[0] {
			m[names[i]] = n
		}
		Logger.Println("1 Stellar Lumen = $" + m["price"])
		response = m["price"]
	}

	return response
}

func getStellarLumens() string {
	response := ""

	stellarLumensURL := fmt.Sprintf("https://horizon.stellar.org/accounts/%s", os.Getenv("LUMENS_ADDRESS"))
	// {"status":"1","message":"OK","result":{"ethbtc":"0.0217","ethbtc_timestamp":"1589119180","ethusd":"190.57","ethusd_timestamp":"1589119172"}}
	stellarResp, err := http.Get(stellarLumensURL)
	if err != nil {
		response = fmt.Sprintf("ERR: stellar Lumens http.Get: %s", err)
	} else {
		defer stellarResp.Body.Close()
		stellarLumensLedger := new(structures.StellarLumensLedger)
		stellarLumensJSON, err2 := io.ReadAll(stellarResp.Body)

		if err2 == nil {
			json.Unmarshal([]byte(stellarLumensJSON), &stellarLumensLedger)
			// find correct Balance -> AssetType == "native"
			for _, balance := range stellarLumensLedger.Balances {
				if balance.AssetType == "native" {
					response = balance.Balance
					break
				}
			}
		} else {
			response = fmt.Sprintf("ERR: stellar Lumens io.ReadAll: %s", err2)
		}
	}

	return response
}

func checkEthereumValue() string {
	response := ""

	// env vars
	var ethAddress = os.Getenv("ETHEREUM_ADDRESS")
	var etherscanAPIKey = os.Getenv("ETHERSCAN_API_KEY")

	accountBalanceStr := getEthereumTokens(ethAddress, etherscanAPIKey) // + " ETH :ethereum:"
	if !strings.HasPrefix(accountBalanceStr, "ERR") {
		ethereumPriceUSDStr := getEthereumPrice(etherscanAPIKey)
		if !strings.HasPrefix(ethereumPriceUSDStr, "ERR") {
			accountBalance, err := strconv.ParseFloat(accountBalanceStr, 64)
			if err == nil {
				ethereumPriceUSD, err := strconv.ParseFloat(ethereumPriceUSDStr, 64)
				if err == nil {
					response = fmt.Sprintf(
						"Your %f %s is worth $%f [etherscan.io](https://etherscan.io/address/%s)",
						accountBalance, ethEmoji, ethereumPriceUSD*accountBalance, ethAddress)
				} else {
					response = err.Error()
				}
			} else {
				response = err.Error()
			}
		} else {
			response = ethereumPriceUSDStr
		}
	} else {
		response = accountBalanceStr
	}

	return response
}

func getEthereumPrice(etherscanAPIKey string) string {
	response := ""

	// https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=EUR => {"EUR":187.32}

	ethereumPriceURL := fmt.Sprintf("https://api.etherscan.io/api?module=stats&action=ethprice&apikey=%s", etherscanAPIKey)
	// {"status":"1","message":"OK","result":{"ethbtc":"0.0217","ethbtc_timestamp":"1589119180","ethusd":"190.57","ethusd_timestamp":"1589119172"}}
	Logger.Println(ethereumPriceURL)
	ethereumResp, err := http.Get(ethereumPriceURL)
	if err != nil {
		response = fmt.Sprintf("ERR: etherscan EthPrice http.Get: %s", err)
	} else {
		defer ethereumResp.Body.Close()
		ethereumPriceJSON, err2 := io.ReadAll(ethereumResp.Body)

		if err2 == nil {
			var result map[string]map[string]string
			json.Unmarshal([]byte(ethereumPriceJSON), &result)
			Logger.Println(result)

			ethereumPrice, err3 := strconv.ParseFloat(string(result["result"]["ethusd"]), 64)
			if err3 == nil {
				response = fmt.Sprintf("%f", ethereumPrice)
			} else {
				response = fmt.Sprintf("ERR: etherscan EthPrice ParseFloat: %s", err3)
			}
		} else {
			response = fmt.Sprintf("ERR: etherscan EthPrice io.ReadAll: %s", err2)
		}
	}

	return response
}

func getEthereumTokens(ethAddress, etherscanAPIKey string) string {
	response := ""
	credentials := fmt.Sprintf("&address=0x%s&apikey=%s",
		ethAddress,
		etherscanAPIKey)

	etherscanAccountBalanceURL := fmt.Sprintf(
		"https://api.etherscan.io/api?module=account&tag=latest&action=balance%s",
		credentials)
	Logger.Println(etherscanAccountBalanceURL)

	accountBalanceWeiResp, err := http.Get(etherscanAccountBalanceURL)
	if err != nil {
		response = fmt.Sprintf("ERR: etherscan AcctBal http.Get: %s", err)
	} else {
		defer accountBalanceWeiResp.Body.Close()
		accountBalanceWeiJSON, err2 := io.ReadAll(accountBalanceWeiResp.Body)
		Logger.Println("Wei response: " + string(accountBalanceWeiJSON))
		if err2 == nil {
			var result map[string]string
			json.Unmarshal([]byte(accountBalanceWeiJSON), &result)
			Logger.Println(result)

			accountBalanceWei, err3 := strconv.ParseFloat(result["result"], 64)
			if err3 == nil {
				accountBalance := accountBalanceWei / 1000000000000000000
				response = fmt.Sprintf("%f", accountBalance)
			} else {
				response = fmt.Sprintf("ERR: etherscan AcctBal ParseFloat: %s", err3)
			}
		} else {
			response = fmt.Sprintf("ERR: etherscan AcctBal io.ReadAll: %s", err2)
		}
	}

	return response
}
