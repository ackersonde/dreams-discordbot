package commands

import (
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	"gitlab.com/ackersonde/dreams-discordbot/structures"
)

func TestRemoteConnectionConfiguration(t *testing.T) {
	tests := []struct {
		name    string
		config  *structures.RemoteConnectConfig
		wantNil bool
	}{
		{
			name: "valid configuration",
			config: &structures.RemoteConnectConfig{
				User:       "testuser",
				HostSSHKey: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0g+ZTxC7weoIJLUafOgrm+h",
			},
			wantNil: false,
		},
		{
			name: "invalid host key",
			config: &structures.RemoteConnectConfig{
				User:       "testuser",
				HostSSHKey: "invalid-key",
			},
			wantNil: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := remoteConnectionConfiguration(tt.config)
			if (got == nil) != tt.wantNil {
				t.Errorf("remoteConnectionConfiguration() got = %v, want nil = %v", got, tt.wantNil)
			}
		})
	}
}

func TestFetchHomeIPv6PrefixRegex(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name: "valid IPv6 prefix",
			input: `NewIPv6Prefix 2001:db8::
NewPrefixLength 64
OtherStuff`,
			expected: "2001:db8::/64",
		},
		{
			name:     "empty input",
			input:    "",
			expected: "",
		},
		{
			name: "malformed input",
			input: `NewIPv6Prefix
NewPrefixLength
OtherStuff`,
			expected: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			re := regexp.MustCompile(`(.*)NewIPv6Prefix (?P<prefix>.*)\nNewPrefixLength (?P<length>.*)\n(.*)`)
			matches := re.FindAllStringSubmatch(tt.input, -1)
			names := re.SubexpNames()

			m := map[string]string{}
			if len(matches) > 0 {
				for i, n := range matches[0] {
					m[names[i]] = n
				}
			}

			var got string
			if len(m) > 1 && m["prefix"] != "" && m["length"] != "" {
				prefix := m["prefix"]
				if !strings.HasSuffix(prefix, "/"+m["length"]) {
					got = prefix + "/" + m["length"]
				} else {
					got = prefix
				}
			}

			if got != tt.expected {
				t.Errorf("IPv6 prefix extraction got = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestGetPublicCertificate(t *testing.T) {
	// Create temporary test files
	tmpDir := t.TempDir()
	privateKeyPath := filepath.Join(tmpDir, "test_key")

	// Test with missing files
	t.Run("missing private key", func(t *testing.T) {
		signer := GetPublicCertificate(privateKeyPath)
		if signer != nil {
			t.Error("Expected nil signer for missing private key")
		}
	})

	// Create test private key
	privateKey := []byte(`-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAQEAvKQeYxu2Jh/mVE9BhcXHbWZOCBfJeYk8Gn+AzFgdZNQCE0QDWV1F
-----END OPENSSH PRIVATE KEY-----`)

	err := os.WriteFile(privateKeyPath, privateKey, 0600)
	if err != nil {
		t.Fatalf("Failed to write test private key: %v", err)
	}

	// Test with invalid private key
	t.Run("invalid private key", func(t *testing.T) {
		signer := GetPublicCertificate(privateKeyPath)
		if signer != nil {
			t.Error("Expected nil signer for invalid private key")
		}
	})

	// Cleanup is handled automatically by t.TempDir()
}

func TestBoldPercentages(t *testing.T) {
	// Test specific cases
	testCases := []struct {
		input    string
		expected string
	}{
		{"40%", "**40%**"},
		{"83%", "**83%**"},
		{"53%", "**53%**"},
		{"No percentage here", "No percentage here"},
		{"Multiple 40% and 83% in one line", "Multiple **40%** and **83%** in one line"},
	}

	for _, tc := range testCases {
		got := boldPercentages(tc.input)
		if got != tc.expected {
			t.Errorf("For input %q, expected %q but got %q", tc.input, tc.expected, got)
		}
	}
}
