package commands

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ackersonde/dreams-discordbot/structures"
)

var proxies = []string{"apibay.org"}

var protonVPNEmoji = "<:protonvpn:1292429910626926663>"

var SearchCommand = discordgo.ApplicationCommand{
	Name:        "search",
	Description: "Search for popular torrents to download",
	Options: []*discordgo.ApplicationCommandOption{{
		Name:        "query",
		Description: "Search for this title from YTS.MX and PirateBay",
		Type:        discordgo.ApplicationCommandOptionString,
		Required:    false,
	}},
}

// handleSearchAsync is the new async version that returns a string result
func handleSearchAsync(opts optionMap) (string, error) {
	builder := new(strings.Builder)

	switch {
	case opts["query"] != nil:
		query := opts["query"].StringValue()
		builder.WriteString("Results from YTS.MX :projector:")
		builder.WriteString(parseMovieTorrents("&query_term=" + url.QueryEscape(query)))

		builder.WriteString("\n\nResults from PirateBay :pirate_flag:\n")
		builder.WriteString(parseTorrents(searchProxy("/q.php?q=" + url.QueryEscape(query))))
	default:
		// else, just run the top 100 search
		builder.WriteString(parseTop100(searchProxy("/precompiled/data_top100_207.json")))
	}

	return builder.String(), nil
}

func searchProxy(url string) []byte {
	var jsonResults []byte

	for i, proxy := range proxies {
		uri := "https://" + proxy + url
		Logger.Printf("porq try #%d: %s ...\n", i, uri)
		req, err := http.NewRequest("GET", uri, nil)
		if err != nil {
			Logger.Printf("http.NewRequest() failed with '%s'\n", err)
			continue
		}

		// create a context indicating 5s timeout
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		req = req.WithContext(ctx)
		resp, err := http.DefaultClient.Do(req)
		if err != nil || resp.StatusCode != http.StatusOK {
			if err != nil {
				Logger.Printf("%s failed with:\n'%s'\n", proxy, err)
			} else {
				Logger.Printf("GET %s failed with '%s'\n", uri, resp.Status)
			}
			continue
		}
		if resp != nil {
			defer resp.Body.Close()
			body, err := io.ReadAll(resp.Body)
			if err != nil || body == nil {
				Logger.Printf("failed to parse JSON: %s", err.Error())
				continue
			}

			return body
		}
	}

	return jsonResults
}

func parseTop100(jsonResponse []byte) string {
	return top100Response(*getTop100FromJSON(jsonResponse))
}

func parseTorrents(jsonResponse []byte) string {
	return torrentResponse(*getTorrentsFromJSON(jsonResponse))
}

func parseMovieTorrents(args string) string {
	results := ""

	// TODO: grab top 30 downloaded movies (hint:`sort_by=seeds` is broken 23.06.2023)
	uri := "https://yts.mx/api/v2/list_movies.json?sort_by=download_count&limit=50"

	if len(args) > 0 {
		uri += args
	}
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		Logger.Printf("YTS.MX http.NewRequest() failed with '%s'\n", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil || resp.StatusCode != http.StatusOK {
		if err != nil {
			Logger.Printf("YTS.MX/api failed with:\n'%s'\n", err)
		} else {
			Logger.Printf("GET %s failed with '%s'\n", uri, resp.Status)
		}
	}

	if resp != nil {
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil || body == nil {
			Logger.Printf("failed to parse JSON: %s", err.Error())
		}

		var ytsMxTopMovies = new(structures.YtsMx)
		marshalErr := json.Unmarshal(body, &ytsMxTopMovies)
		if marshalErr != nil {
			Logger.Printf("ERR: %s", marshalErr)
		}
		for _, movie := range ytsMxTopMovies.Data.Movies {
			torrentsFound := false
			for _, torrent := range movie.Torrents {
				if torrent.Seeds > 20 && (torrent.Quality == "1080p" || torrent.Quality == "2160p") {
					if !torrentsFound {
						results += fmt.Sprintf("\n- **[%s](https://www.imdb.com/title/%s) %d**\n", movie.Title, movie.ImdbCode, movie.Year)
						torrentsFound = true
					}
					results += prepareMultiLinks(torrent.Hash, torrent.Seeds, movie.ImdbCode, torrent.Size, movie.Title, torrent.Quality)
				}
			}
		}
	}

	return results
}

func top100Response(top100 structures.Top100Movies) string {
	var response string

	for i, torrent := range top100 {
		if torrent.Seeders > 10 {
			if torrent.Imdb == nil {
				torrent.Imdb = ""
			}
			response += prepareLink(
				i, torrent.InfoHash, torrent.Name,
				torrent.Seeders, calculateHumanSize(torrent.Size),
				torrent.Imdb.(string), "")
		}
	}

	if response == "" {
		return "Unable to find torrents for your search"
	}

	return response
}

func torrentResponse(torrents structures.Torrents) string {
	var response string

	for i, torrent := range torrents {
		seeders, err2 := strconv.Atoi(torrent.Seeders)
		if err2 != nil {
			Logger.Printf("ERR torrent seeder Atoi: %s\n", err2.Error())
			continue
		}
		size, err3 := strconv.ParseUint(torrent.Size, 10, 64)
		if err3 != nil {
			Logger.Printf("ERR torrent size Atoi: %s\n", err3.Error())
			continue
		}

		if seeders > 10 {
			response += prepareLink(
				i, torrent.InfoHash, torrent.Name,
				seeders, calculateHumanSize(size), torrent.Imdb, "")
		}
	}

	if response == "" {
		return "Unable to find torrents for your search"
	}
	return response
}

func calculateHumanSize(size uint64) string {
	humanSize := float64(size / (1024 * 1024))
	sizeSuffix := fmt.Sprintf("*%.0f MiB*", humanSize)
	if humanSize > 999 {
		humanSize = humanSize / 1024
		sizeSuffix = fmt.Sprintf("*%.1f GiB*", humanSize)
	}
	return sizeSuffix
}

func cleanTorrentName(name string) string {
	// Replace square brackets with dashes
	name = strings.NewReplacer(
		"[", "-",
		"]", "-",
	).Replace(name)

	// Trim non-ASCII characters
	return strings.Map(func(r rune) rune {
		if r > 127 {
			return -1 // Drop the character
		}
		return r
	}, name)
}

func prepareMultiLinks(
	magnetLink string, torrentSeeders int, imdb string,
	sizeSuffix string, torrentName string, torrentQuality string) string {
	var response string

	torrentName = cleanTorrentName(torrentName)

	response += fmt.Sprintf("└ [%s](https://"+os.Getenv("HOME_API_HOSTNAME")+
		"/qbt/add?hash=%s&name=%s&imdb=%s&token=%s): Seeds:%d %s\n",
		torrentQuality, magnetLink, sanitizeTorrentName(torrentName),
		imdb, os.Getenv("HOME_QBT_API_TOKEN"), torrentSeeders, sizeSuffix)

	return response
}

func sanitizeTorrentName(s string) string {
	// Find the first occurrence of quality suffixes and trim the string there
	qualities := []string{"720p", "1080p", "2160p"}
	minIndex := len(s)

	for _, quality := range qualities {
		if idx := strings.Index(strings.ToLower(s), strings.ToLower(quality)); idx != -1 && idx < minIndex {
			minIndex = idx
		}
	}

	// Trim the string at the earliest quality suffix
	if minIndex < len(s) {
		s = strings.TrimSpace(s[:minIndex])
	}

	// Use Go's standard url.QueryEscape to properly encode the string for use in URLs
	return url.QueryEscape(s)
}

func prepareLink(i int, magnetLink string, torrentName string,
	torrentSeeders int, sizeSuffix string, imdb string, quality string) string {
	var response string

	torrentName = cleanTorrentName(torrentName)

	response += fmt.Sprintf(
		"%d: [%s](https://%s/qbt/add?hash=%s&name=%s&imdb=%s&token=%s) Seeds:%d %s %s",
		i, torrentName, os.Getenv("HOME_API_HOSTNAME"),
		magnetLink, sanitizeTorrentName(torrentName), imdb, os.Getenv("HOME_QBT_API_TOKEN"),
		torrentSeeders, quality, sizeSuffix)

	if imdb != "" {
		response += fmt.Sprintf(" ([imdb](https://www.imdb.com/title/%s))", imdb)
	}

	return response + "\n"
}

func getTorrentsFromJSON(jsonObject []byte) *structures.Torrents {
	var s = new(structures.Torrents)
	err := json.Unmarshal(jsonObject, &s)
	if err != nil {
		Logger.Printf("ERR: %s => %s", err, jsonObject)
	}

	return s
}

func getTop100FromJSON(jsonObject []byte) *structures.Top100Movies {
	var s = new(structures.Top100Movies)
	err := json.Unmarshal(jsonObject, &s)
	if err != nil {
		Logger.Printf("ERR: %s => %s", err, jsonObject)
	}

	return s
}
